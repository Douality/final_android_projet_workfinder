package com.example.projectmobile.Model;

import com.google.gson.annotations.SerializedName;

public class CandidateResponse {
    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("id")
    private String candidate_id;

    @SerializedName("candidat_info")
    private Candidate candidate_info;

    public CandidateResponse() {};

    public CandidateResponse(int code, String message, String candidate_id, Candidate candidate_info) {
        this.code = code;
        this.message = message;
        this.candidate_id = candidate_id;
        this.candidate_info = candidate_info;
    }

    public Candidate getCandidate_info() {
        return candidate_info;
    }

    public void setCandidate_info(Candidate candidate_info) {
        this.candidate_info = candidate_info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCandidate_id() {
        return candidate_id;
    }

    public void setCandidate_id(String candidate_id) {
        this.candidate_id = candidate_id;
    }
}
