package com.example.projectmobile.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.projectmobile.Activity.JobDetailsActivity;
import com.example.projectmobile.Activity.MainActivity;
import com.example.projectmobile.Activity.UpdateCvActivity;
import com.example.projectmobile.Model.CV;
import com.example.projectmobile.Model.CVResponse;
import com.example.projectmobile.Model.JobOfferResponse;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.CVService;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListCvAdapter extends ArrayAdapter<CV> {
    private Context context;
    private List<CV> cvs;
    private CVService cvService;

    public ListCvAdapter(@NonNull Context context, int resource, @NonNull List<CV> objects) {
        super(context, resource, objects);
        this.context = context;
        this.cvs = objects;
        cvService = APIUtils.getCVService();
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_cv, parent, false);

        //init variable
        TextView tv_title = rowView.findViewById(R.id.tv_cv_child_title);
        ImageButton btn_edit = rowView.findViewById(R.id.btn_cv_child_edit);
        ImageButton btn_delete = rowView.findViewById(R.id.btn_cv_child_delete);

        //set data
        tv_title.setText(cvs.get(pos).getName());

        //set event listener
        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, UpdateCvActivity.class);
                intent.putExtra("cv_info", cvs.get(pos));
                context.startActivity(intent);
            }
        });

        btn_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure to delete this cv?").setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                DeleteCV(pos);
                            }
                        })
                        .setNegativeButton("No", null).show();
            }
        });

        return rowView;
    }

    void DeleteCV(int pos) {

        Call<CVResponse> call = cvService.deleteCV(cvs.get(pos).getId());
        call.enqueue(new Callback<CVResponse>() {
            @Override
            public void onResponse(Call<CVResponse> call, Response<CVResponse> response) {
                if(response.isSuccessful()){
                    Toast.makeText(context, "Delete cv successfully!", Toast.LENGTH_SHORT).show();
                    cvs.remove(pos);
                    notifyDataSetChanged();
                } else {
                    Toast.makeText(context, "Delete cv fail!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CVResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }
}
