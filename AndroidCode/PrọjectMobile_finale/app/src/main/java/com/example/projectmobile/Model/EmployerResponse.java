package com.example.projectmobile.Model;

import com.google.gson.annotations.SerializedName;

public class EmployerResponse {
    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("id")
    private String employer_id;

    @SerializedName("employeur_info")
    private Employer employer_info;

    public EmployerResponse() {};

    public EmployerResponse(int code, String message, String employer_id, Employer employer_info) {
        this.code = code;
        this.message = message;
        this.employer_id = employer_id;
        this.employer_info = employer_info;
    }

    public Employer getEmployer_info() {
        return employer_info;
    }

    public void setEmployer_info(Employer employer_info) {
        this.employer_info = employer_info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmployer_id() {
        return employer_id;
    }

    public void setEmployer_id(String employer_id) {
        this.employer_id = employer_id;
    }
}
