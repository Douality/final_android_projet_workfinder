package com.example.projectmobile.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserAbonnement {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("employeur_id")
    @Expose
    private String employeur_id;

    @SerializedName("abonnement_id")
    @Expose
    private String abonnement_id;

    @SerializedName("start_date")
    @Expose
    private long start_date;

    public UserAbonnement() {
    }

    public UserAbonnement(String id, String employeur_id, String abonnement_id, long start_date) {
        this.id = id;
        this.employeur_id = employeur_id;
        this.abonnement_id = abonnement_id;
        this.start_date = start_date;
    }

    public UserAbonnement(String employeur_id, String abonnement_id) {
        this.employeur_id = employeur_id;
        this.abonnement_id = abonnement_id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getEmployeur_id() {
        return employeur_id;
    }

    public void setEmployeur_id(String employeur_id) {
        this.employeur_id = employeur_id;
    }

    public String getAbonnement_id() {
        return abonnement_id;
    }

    public void setAbonnement_id(String abonnement_id) {
        this.abonnement_id = abonnement_id;
    }

    public long getStart_date() {
        return start_date;
    }

    public void setStart_date(long start_date) {
        this.start_date = start_date;
    }
}
