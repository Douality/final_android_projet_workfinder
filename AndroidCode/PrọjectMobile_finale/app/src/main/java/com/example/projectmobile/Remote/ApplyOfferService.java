package com.example.projectmobile.Remote;

import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.Model.ApplyOfferResponse;
import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.CandidateResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface ApplyOfferService {
    @POST("jobOffer/apply")
    Call<ApplyOfferResponse> applyJobOffer(@Body ApplyOffer applyOffer);

    @PUT("jobOffer/response/{id}")
    Call<ApplyOfferResponse> responseCandidature(@Path("id") String id, @Body ApplyOffer applyOffer);

    @GET("jobOffer/getAppliedJob/{id}")
    Call<ApplyOfferResponse> getAppliedOffer(@Path("id") String id);

    @GET("jobOffer/getAppliedCandidatures/{id}")
    Call<ApplyOfferResponse> getAppliedCandidatures(@Path("id") String id);
}
