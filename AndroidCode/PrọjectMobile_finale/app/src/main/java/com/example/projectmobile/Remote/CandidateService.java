package com.example.projectmobile.Remote;

import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.CandidateResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CandidateService {

    @POST("candidat/login")
    Call<CandidateResponse> loginCandidate(@Body Candidate candidate);

    @POST("candidat/register")
    Call<CandidateResponse> registerCandidate(@Body Candidate candidate);

    @PUT("candidat/update/{id}")
    Call<CandidateResponse> updateCandidate(@Path("id") String id, @Body Candidate Candidate);

    @DELETE("candidat/delete/{id}")
    Call<CandidateResponse> deleteCategory(@Path("id") String id);
}
