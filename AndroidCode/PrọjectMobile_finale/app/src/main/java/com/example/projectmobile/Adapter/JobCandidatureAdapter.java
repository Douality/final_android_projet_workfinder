package com.example.projectmobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.projectmobile.Activity.ResponseCandidatureActivity;
import com.example.projectmobile.Activity.ViewCandidatDetailsActivity;
import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.R;

import java.util.List;

public class JobCandidatureAdapter extends ArrayAdapter<ApplyOffer> {
    private Context context;
    private List<ApplyOffer> applyOffers;

    public JobCandidatureAdapter(@NonNull Context context, int resource, @NonNull List<ApplyOffer> objects) {
        super(context, resource, objects);
        this.applyOffers = objects;
        this.context = context;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_candidature, parent, false);

        //init variable
        TextView tv_title = rowView.findViewById(R.id.tv_candidature_name);
        TextView tv_status = rowView.findViewById(R.id.tv_candidature_status);
        ImageButton btn_edit = rowView.findViewById(R.id.btn_candidature_edit);
        LinearLayout ll_candidature = rowView.findViewById(R.id.ll_candidature);
        //set data
        tv_title.setText(applyOffers.get(pos).getCandidat().getFull_name());
        int status = applyOffers.get(pos).getApply_status();
        if(status == 0) {
            tv_status.setText("waiting");
            tv_status.setTextColor(context.getColor(R.color.yellow));
        } else if(status == 1) {
            tv_status.setText("accepted");
            tv_status.setTextColor(context.getColor(R.color.green_light));
        } else if(status == 2) {
            tv_status.setText("refused");
            tv_status.setTextColor(context.getColor(R.color.red));
        }

        //event listener
        ll_candidature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ViewCandidatDetailsActivity.class);
                intent.putExtra("candidature_offer", applyOffers.get(pos));
                context.startActivity(intent);
            }
        });

        btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, ResponseCandidatureActivity.class);
                intent.putExtra("candidature_offer", applyOffers.get(pos));
                context.startActivity(intent);
            }
        });

        return rowView;
    }
}
