package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.CandidateResponse;
import com.example.projectmobile.Model.Employer;
import com.example.projectmobile.Model.EmployerResponse;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.CandidateService;
import com.example.projectmobile.Remote.EmployerService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateUserActivity extends AppCompatActivity {

    CandidateService candidateService;
    EmployerService employerService;
    Employer e_info;
    Candidate c_info;
    EditText et_fullname, et_phone, et_email, et_location_city, et_location_country, et_company_name, et_company_web;
    Button btn_submit;
    long user_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_user);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //init variables
        candidateService = APIUtils.getCandidateService();
        employerService = APIUtils.getEmployerService();
        et_fullname = findViewById(R.id.et_update_info_full_name);
        et_phone = findViewById(R.id.et_update_info_phone);
        et_email = findViewById(R.id.et_update_info_email);
        et_location_city = findViewById(R.id.et_location_city_update_info);
        et_location_country = findViewById(R.id.et_location_country_update_info);
        et_company_name = findViewById(R.id.et_update_info_company_name);
        et_company_web = findViewById(R.id.et_update_info_company_web);
        btn_submit = findViewById(R.id.btn_submit_update_info);
        user_type = ((MyApplication)getApplication()).getUserType();

        //set data
        UpdateUIWithUserType();

        //set event listener
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hasEmptyFields()) {
                    Toast.makeText(UpdateUserActivity.this, "Please fill all the required fields.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(user_type == 0) {
                    c_info.setFull_name(et_fullname.getText().toString());
                    c_info.setEmail(et_email.getText().toString());
                    c_info.setPhone(et_phone.getText().toString());
                    c_info.setLocation_city(et_location_city.getText().toString());
                    c_info.setLocation_country(et_location_country.getText().toString());
                    updateCandidate(c_info);
                }

                if(user_type == 1) {
                    e_info.setFull_name(et_fullname.getText().toString());
                    e_info.setEmail(et_email.getText().toString());
                    e_info.setPhone(et_phone.getText().toString());
                    e_info.setLocation_city(et_location_city.getText().toString());
                    e_info.setLocation_country(et_location_country.getText().toString());
                    e_info.setCompany_name(et_company_name.getText().toString());
                    e_info.setCompany_web(et_company_web.getText().toString());
                    updateEmployer(e_info);
                }
            }
        });
    }

    private boolean hasEmptyFields() {
        if(et_email.getText().toString().equals("")) { return true; }
        if(et_fullname.getText().toString().equals("")) { return true; }
        if(et_phone.getText().toString().equals("")) {return true;}

        return false;
    }

    private void UpdateUIWithUserType() {
        if(user_type == 0) {
            UpdateCandidatUI();
        }

        if(user_type == 1) {
            UpdateEmployerUI();
        }
    }

    private void UpdateEmployerUI() {
        et_location_city.setHint("Company city");
        et_location_country.setHint("Company country");
        //
        e_info = ((MyApplication)getApplication()).getEmployer_info();
        et_fullname.setText(e_info.getFull_name());
        et_email.setText(e_info.getEmail());
        et_phone.setText(e_info.getPhone());
        et_location_city.setText(e_info.getLocation_city());
        et_location_country.setText(e_info.getLocation_country());
        et_company_name.setText(e_info.getCompany_name());
        et_company_web.setText(e_info.getCompany_web());
    }

    private void UpdateCandidatUI() {
        et_company_name.setVisibility(View.GONE);
        et_company_web.setVisibility(View.GONE);
        et_location_city.setHint("City");
        et_location_country.setHint("Country");
        //
        c_info = ((MyApplication)getApplication()).getCandidate_info();
        et_fullname.setText(c_info.getFull_name());
        et_email.setText(c_info.getEmail());
        et_phone.setText(c_info.getPhone());
        et_location_city.setText(c_info.getLocation_city());
        et_location_country.setText(c_info.getLocation_country());
    }

    private void onUpdateSuccess(Employer update_e, Candidate update_c) {
        ((MyApplication)getApplication()).setEmployer_info(update_e);
        ((MyApplication)getApplication()).setCandidate_info(update_c);
        //redirect
        Intent intent = new Intent(UpdateUserActivity.this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("message", "Update information successfully !");
        bundle.putInt("tab", 2);
        startActivity(intent.putExtras(bundle));
    }

    private void updateCandidate(Candidate c) {
        Call<CandidateResponse> call = candidateService.updateCandidate(c.getId(), c);
        call.enqueue(new Callback<CandidateResponse>() {
            @Override
            public void onResponse(Call<CandidateResponse> call, Response<CandidateResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 200) {
                        onUpdateSuccess(new Employer(), response.body().getCandidate_info());
                    } else {
                        Toast.makeText(UpdateUserActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(UpdateUserActivity.this, "Request error.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CandidateResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    private void updateEmployer(Employer e) {
        Call<EmployerResponse> call = employerService.updateEmployer(e_info.getId(), e);
        call.enqueue(new Callback<EmployerResponse>() {
            @Override
            public void onResponse(Call<EmployerResponse> call, Response<EmployerResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 200) {
                        onUpdateSuccess(response.body().getEmployer_info(), new Candidate());
                    } else {
                        Toast.makeText(UpdateUserActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(UpdateUserActivity.this, "Request error.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EmployerResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}