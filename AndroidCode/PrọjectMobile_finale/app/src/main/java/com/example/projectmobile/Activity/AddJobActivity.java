package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;


import com.example.projectmobile.Model.Category;
import com.example.projectmobile.Model.CategoryResponse;
import com.example.projectmobile.Model.Employer;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.Model.JobOfferResponse;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.CategoryService;
import com.example.projectmobile.Remote.JobOfferService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddJobActivity extends AppCompatActivity {

    CategoryService categoryService;
    JobOfferService jobOfferService;
    List<Category> list_domains = new ArrayList<Category>();
    Spinner spinner_domain;
    EditText et_title, et_description, et_salary, et_city, et_country, et_exp, et_requirement, et_duration;
    Button btn_submit;

    Integer require_length = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_job);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //init variable
        categoryService = APIUtils.getCategoryService();
        jobOfferService = APIUtils.getJobOfferService();
        spinner_domain = findViewById(R.id.spin_domain_add_job);
        et_title = findViewById(R.id.et_title_add_job);
        et_description = findViewById(R.id.et_description_add_job);
        et_salary = findViewById(R.id.et_salary_add_job);
        et_city = findViewById(R.id.et_location_city_add_job);
        et_country = findViewById(R.id.et_location_country_add_job);
        et_exp = findViewById(R.id.et_exp_add_job);
        et_requirement = findViewById(R.id.et_requirement_add_job);
        et_duration = findViewById(R.id.et_duration_add_job);
        btn_submit = findViewById(R.id.btn_submit_add_job);

        if(list_domains.size() == 0) {
            getCategoriesList();
        } else {
            updateSpinnerDomain();
        }

        //add event listener
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hasEmptyFields()) {
                    Toast.makeText(AddJobActivity.this, "Please fill all required fields.", Toast.LENGTH_SHORT).show();
                    return;
                }

                addJobOffer();
            }
        });

        et_requirement.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                // Add bullet points when the user starts a new line

                if(require_length < charSequence.length()) {
                    String[] lines = charSequence.toString().split("\n");
                    for (int i = 0; i < lines.length; i++) {
                        if (!lines[i].startsWith(getString(R.string.bullet))) {
                            lines[i] = getString(R.string.bullet) + " " + lines[i];
                        }
                    }

                    String newText = TextUtils.join("\n", lines);
                    if(charSequence.charAt(charSequence.length() - 1) == '\n') {
                        newText += "\n" + getString(R.string.bullet) + " ";
                    }

                    if (!newText.equals(et_requirement.getText().toString())) {
                        // The text has actually changed, so we can safely set it
                        et_requirement.setText(newText);
                        et_requirement.setSelection(newText.length());
                    }

                }
                require_length = charSequence.length();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }

    private boolean hasEmptyFields() {
        if(spinner_domain.getSelectedItemId() == 0) {return true;}
        if(et_title.getText().toString().equals("")) { return true; }
        if(et_description.getText().toString().equals("")) { return true; }
        if(et_salary.getText().toString().equals("")) { return true; }
        if(et_city.getText().toString().equals("")) { return true; }
        if(et_country.getText().toString().equals("")) { return true; }
        if(et_exp.getText().toString().equals("")) { return true; }
        if(et_requirement.getText().toString().equals("")) { return true; }
        if(et_duration.getText().toString().equals("")) { return true; }

        return false;
    }

    private void onAddJobSuccess() {
        Intent intent = new Intent(AddJobActivity.this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("message", "Create job offer successfully!");

        startActivity(intent.putExtras(bundle));
    }

    private void addJobOffer() {
        Employer employer = ((MyApplication)this.getApplication()).getEmployer_info();
        Category category = list_domains.get((int) spinner_domain.getSelectedItemId());
        String title = et_title.getText().toString();
        String description = et_description.getText().toString();
        Long salary = Long.valueOf(et_salary.getText().toString());
        String city = et_city.getText().toString();
        String country = et_country.getText().toString();
        String requirement = et_requirement.getText().toString();
        Long exp = Long.valueOf(et_exp.getText().toString());
        Long duration = Long.valueOf(et_duration.getText().toString());

        JobOffer j = new JobOffer(employer, category, title, description, city, country, salary, exp, requirement, duration);

        Call<JobOfferResponse> call = jobOfferService.addJobOffer(j);
        call.enqueue(new Callback<JobOfferResponse>() {
            @Override
            public void onResponse(Call<JobOfferResponse> call, Response<JobOfferResponse> response) {
                if(response.isSuccessful()){
                    onAddJobSuccess();
                } else {
                    Toast.makeText(AddJobActivity.this, "Job offer created fail!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<JobOfferResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    public void updateSpinnerDomain() {
        ArrayAdapter<Category> domainAdapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, list_domains);
        domainAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner_domain.setAdapter(domainAdapter);
    }

    public void getCategoriesList(){
        Call<CategoryResponse> call = categoryService.getCategories();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                if(response.isSuccessful()){
                    list_domains.add(new Category("0", "Select a field of work"));
                    list_domains.addAll(response.body().getCategories());
                    //update to spinner
                    updateSpinnerDomain();
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}