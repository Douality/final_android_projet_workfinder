package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.ApplyOfferService;
import com.example.projectmobile.Remote.CVService;

public class ViewCandidatDetailsActivity extends AppCompatActivity {

    ApplyOfferService applyOfferService;
    TextView tv_name, tv_email, tv_phone, tv_address, tv_study, tv_skill, tv_exp, tv_other, tv_motivation, tv_question;
    Button btn_response;
    ApplyOffer applyOffer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_candidat_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //init variable
        applyOfferService = APIUtils.getApplyOfferService();
        tv_name = findViewById(R.id.tv_name_candidature_details);
        tv_email = findViewById(R.id.tv_email_candidature_details);
        tv_phone = findViewById(R.id.tv_phone_candidature_details);
        tv_address = findViewById(R.id.tv_address_candidature_details);
        tv_study = findViewById(R.id.tv_learning_candidature_details);
        tv_skill = findViewById(R.id.tv_skill_candidature_details);
        tv_exp = findViewById(R.id.tv_experience_candidature_details);
        tv_other = findViewById(R.id.tv_others_candidature_details);
        tv_motivation = findViewById(R.id.tv_motivations_candidature_details);
        tv_question = findViewById(R.id.tv_question_candidature_details);
        btn_response = findViewById(R.id.btn_response_candidature);
        applyOffer = (ApplyOffer) getIntent().getSerializableExtra("candidature_offer");

        //set data
        tv_name.setText(applyOffer.getCandidat().getFull_name());
        tv_email.setText(applyOffer.getCandidat().getEmail());
        tv_phone.setText(applyOffer.getCandidat().getPhone());
        tv_address.setText(applyOffer.getCandidat().getLocation_city() + ", " + applyOffer.getCandidat().getLocation_country());
        tv_study.setText(applyOffer.getCv().getStudy_progress());
        tv_skill.setText(applyOffer.getCv().getSkills());
        tv_exp.setText(applyOffer.getCv().getExperiences());
        tv_other.setText(applyOffer.getCv().getOthers());
        tv_motivation.setText(applyOffer.getLetter_motivation());
        tv_question.setText(applyOffer.getAutre_question());

        //set listener
        btn_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewCandidatDetailsActivity.this, ResponseCandidatureActivity.class);
                intent.putExtra("candidature_offer", applyOffer);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}