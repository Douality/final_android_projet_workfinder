package com.example.projectmobile.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CVResponse {
    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("list")
    private List<CV> cvs;

    @SerializedName("cv_info")
    private CV cv_info;

    public CVResponse() {
    }

    public CVResponse(int code, String message, List<CV> cvs, CV cv_info) {
        this.code = code;
        this.message = message;
        this.cvs = cvs;
        this.cv_info = cv_info;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<CV> getCvs() {
        return cvs;
    }

    public void setCvs(List<CV> cvs) {
        this.cvs = cvs;
    }

    public CV getCv_info() {
        return cv_info;
    }

    public void setCv_info(CV cv_info) {
        this.cv_info = cv_info;
    }
}
