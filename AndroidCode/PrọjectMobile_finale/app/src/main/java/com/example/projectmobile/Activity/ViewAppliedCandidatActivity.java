package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.JobCandidatureAdapter;
import com.example.projectmobile.Adapter.YourApplyOfferAdapter;
import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.Model.ApplyOfferResponse;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.ApplyOfferService;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAppliedCandidatActivity extends AppCompatActivity {

    ApplyOfferService applyOfferService;
    List<ApplyOffer> applyOffers;
    ListView lv_candidature;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_applied_candidat);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //announce
        try {
            String message = this.getIntent().getExtras().getString("message");
            if(message != null) {
                Toast.makeText(ViewAppliedCandidatActivity.this, message, Toast.LENGTH_SHORT).show();
                this.getIntent().removeExtra("message");
            }

        } catch (Exception e) {}

        //init variable
        applyOfferService = APIUtils.getApplyOfferService();
        lv_candidature = findViewById(R.id.lv_all_your_candidature);
        GetAllCandidature();
    }

    void GetAllCandidature() {
        String offer_id = getIntent().getStringExtra("offer_id");
        Call<ApplyOfferResponse> call = applyOfferService.getAppliedCandidatures(offer_id);
        call.enqueue(new Callback<ApplyOfferResponse>() {
            @Override
            public void onResponse(Call<ApplyOfferResponse> call, Response<ApplyOfferResponse> response) {
                if(response.isSuccessful()){
                    applyOffers = response.body().getApplyOffers();
                    Collections.reverse(applyOffers);
                    lv_candidature.setAdapter(new JobCandidatureAdapter(ViewAppliedCandidatActivity.this, R.layout.list_cv, applyOffers));
                }
            }

            @Override
            public void onFailure(Call<ApplyOfferResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}