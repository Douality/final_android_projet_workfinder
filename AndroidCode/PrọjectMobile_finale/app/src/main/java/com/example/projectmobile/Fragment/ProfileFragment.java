package com.example.projectmobile.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.projectmobile.Activity.AddJobActivity;
import com.example.projectmobile.Activity.LoginActivity;
import com.example.projectmobile.Activity.MainActivity;
import com.example.projectmobile.Activity.RegisterActivity;
import com.example.projectmobile.Activity.SubscriptionActivity;
import com.example.projectmobile.Activity.UpdateUserActivity;
import com.example.projectmobile.Activity.ViewAllCVActivity;
import com.example.projectmobile.Activity.YourJobOfferActivity;
import com.example.projectmobile.Model.Employer;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;

public class ProfileFragment extends Fragment {

    Button btn_login, btn_register, btn_update_user_info, btn_subscription, btn_announce, btn_your_jobs, btn_logout, btn_add_job, btn_view_cv;
    TextView tv_job_info, tv_description;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        //init variables
        btn_login = view.findViewById(R.id.btn_login);
        btn_register = view.findViewById(R.id.btn_register_main);
        btn_subscription = view.findViewById(R.id.btn_subscription);
        btn_update_user_info = view.findViewById(R.id.btn_update_info_main);
        btn_announce = view.findViewById(R.id.btn_announce_main);
        btn_your_jobs = view.findViewById(R.id.btn_your_job_main);
        btn_logout = view.findViewById(R.id.btn_sign_out_main);
        btn_add_job = view.findViewById(R.id.btn_add_job_main);
        btn_view_cv = view.findViewById(R.id.btn_view_cv_main);

        tv_job_info = view.findViewById(R.id.tv_job_info);
        tv_description = view.findViewById(R.id.tv_description_profile);

        long user_type = ((MyApplication) this.getActivity().getApplication()).getUserType();
        //update UI with user type
        if(user_type == 0) {
            UpdateUICandidat();
        } else if(user_type == 1) {
            UpdateUIEmployer();
        } else {
            UpdateUIAnonymous();
        }



        //add event listener
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), LoginActivity.class);
                startActivity(intent);
            }
        });

        btn_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), RegisterActivity.class);
                startActivity(intent);
            }
        });

        btn_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), SubscriptionActivity.class);
                startActivity(intent);
            }
        });

        btn_update_user_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), UpdateUserActivity.class);
                startActivity(intent);
            }
        });

        btn_view_cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), ViewAllCVActivity.class);
                startActivity(intent);
            }
        });

        btn_your_jobs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), YourJobOfferActivity.class);
                startActivity(intent);
            }
        });

        btn_add_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Employer e_info = ((MyApplication)getActivity().getApplication()).getEmployer_info();

                if(e_info.getCompany_name() == null || e_info.getCompany_name().equals("")) {
                    Toast.makeText(view.getContext(), "You have to update your company name and address first.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(e_info.getLocation_city() == null || e_info.getLocation_city().equals("")) {
                    Toast.makeText(view.getContext(), "You have to update your company name and address first.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(e_info.getLocation_country() == null || e_info.getLocation_country().equals("")) {
                    Toast.makeText(view.getContext(), "You have to update your company name and address first.", Toast.LENGTH_SHORT).show();
                    return;
                }

                Intent intent = new Intent(view.getContext(), AddJobActivity.class);
                startActivity(intent);
            }
        });

        btn_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //update user type
                ((MyApplication) getActivity().getApplication()).setUserType(-1);
                getActivity().getIntent().putExtra("message", "Logout successfully!");
                getActivity().recreate();
            }
        });
    }

    private void UpdateUIEmployer() {
        ((View)btn_login.getParent()).setVisibility(View.GONE);
        ((View)btn_register.getParent()).setVisibility(View.GONE);
        ((View)btn_view_cv.getParent()).setVisibility(View.GONE);
        //
        tv_description.setText("Our employer");
    }

    private void UpdateUICandidat() {
        ((View)btn_login.getParent()).setVisibility(View.GONE);
        ((View)btn_register.getParent()).setVisibility(View.GONE);
        ((View)btn_subscription.getParent()).setVisibility(View.GONE);
        ((View)btn_add_job.getParent()).setVisibility(View.GONE);

        //
        tv_description.setText("Our candidate");
    }

    private void UpdateUIAnonymous() {
        ((View)btn_subscription.getParent()).setVisibility(View.GONE);
        ((View)btn_update_user_info.getParent()).setVisibility(View.GONE);
        ((View)btn_view_cv.getParent()).setVisibility(View.GONE);
        tv_job_info.setVisibility(View.GONE);
        ((View)btn_announce.getParent()).setVisibility(View.GONE);
        ((View)btn_your_jobs.getParent()).setVisibility(View.GONE);
        ((View)btn_add_job.getParent()).setVisibility(View.GONE);
        ((View)btn_logout.getParent()).setVisibility(View.GONE);

        //
        tv_description.setText("You haven't sign in yet");
    }
}
