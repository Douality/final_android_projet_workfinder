package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.SubscriptionAdapter;
import com.example.projectmobile.Adapter.YourApplyOfferAdapter;
import com.example.projectmobile.Model.Abonnement;
import com.example.projectmobile.Model.AbonnementResponse;
import com.example.projectmobile.Model.ApplyOfferResponse;
import com.example.projectmobile.Model.UserAbonnement;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.AbonnementService;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubscriptionActivity extends AppCompatActivity {

    public String selected_id = "";
    AbonnementService abonnementService;
    List<Abonnement> abonnements;
    ListView lv_abonnement_plan;
    TextView tv_label, tv_expire;
    Button btn_subscription;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subsciption);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //announce
        try {
            String message = this.getIntent().getExtras().getString("message");
            if(message != null) {
                Toast.makeText(SubscriptionActivity.this, message, Toast.LENGTH_SHORT).show();
                this.getIntent().removeExtra("message");
            }

        } catch (Exception e) {}

        //init variable
        abonnementService = APIUtils.getAbonnementService();
        lv_abonnement_plan = findViewById(R.id.lv_subscription);
        tv_label = findViewById(R.id.tv_label);
        tv_expire = findViewById(R.id.tv_subscription_expire);
        lv_abonnement_plan.setScrollContainer(false);
        btn_subscription = findViewById(R.id.btn_submit_subscription);
        CheckAbonnement();

        //event listener
        btn_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String abonnement_id = ((SubscriptionAdapter)lv_abonnement_plan.getAdapter()).selected_id;
                if(abonnement_id.equals("")) {
                    Toast.makeText(SubscriptionActivity.this,"Please select a plan!", Toast.LENGTH_SHORT ).show();
                } else {
                    MakeAbonnement(abonnement_id);
                }
            }
        });
    }

    void MakeAbonnement(String abonnement_id) {
        String user_id = ((MyApplication)getApplication()).getUserId();
        UserAbonnement userAbonnement = new UserAbonnement(user_id, abonnement_id);
        Call<AbonnementResponse> call = abonnementService.makeAbonnement(userAbonnement);
        call.enqueue(new Callback<AbonnementResponse>() {
            @Override
            public void onResponse(Call<AbonnementResponse> call, Response<AbonnementResponse> response) {
                if(response.isSuccessful()){
                   OnMakeAbonnementSuccess();
                } else {
                    Toast.makeText(SubscriptionActivity.this,"Subscription fail!", Toast.LENGTH_SHORT ).show();
                }
            }

            @Override
            public void onFailure(Call<AbonnementResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    private void OnMakeAbonnementSuccess() {
        getIntent().putExtra("message", "Subscription successfully!");
        recreate();
    }

    void CheckAbonnement() {
        String user_id = ((MyApplication)getApplication()).getUserId();
        Call<AbonnementResponse> call = abonnementService.checkAbonnement(user_id);
        call.enqueue(new Callback<AbonnementResponse>() {
            @Override
            public void onResponse(Call<AbonnementResponse> call, Response<AbonnementResponse> response) {
                if(response.isSuccessful()){
                    tv_expire.setTextColor(getColor(R.color.green_light));
                    tv_expire.setText(response.body().getOutDateTime());
                } else {
                    lv_abonnement_plan.setVisibility(View.VISIBLE);
                    btn_subscription.setVisibility(View.VISIBLE);
                    tv_label.setVisibility(View.VISIBLE);
                    tv_expire.setText("Your subscription has been expired");
                    tv_expire.setTextColor(getColor(R.color.red));
                    GetAbonnement();
                }
            }

            @Override
            public void onFailure(Call<AbonnementResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    void GetAbonnement() {

        Call<AbonnementResponse> call = abonnementService.getAllAbonnement();
        call.enqueue(new Callback<AbonnementResponse>() {
            @Override
            public void onResponse(Call<AbonnementResponse> call, Response<AbonnementResponse> response) {
                if(response.isSuccessful()){
                    abonnements = response.body().getAbonnements();
                    lv_abonnement_plan.setAdapter(new SubscriptionAdapter(SubscriptionActivity.this, R.layout.list_subscription, abonnements));
                }
            }

            @Override
            public void onFailure(Call<AbonnementResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}