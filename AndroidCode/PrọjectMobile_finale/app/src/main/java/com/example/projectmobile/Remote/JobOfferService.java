package com.example.projectmobile.Remote;

import com.example.projectmobile.Model.Category;
import com.example.projectmobile.Model.CategoryResponse;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.Model.JobOfferResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface JobOfferService {
    @GET("jobOffer/getAll")
    Call<JobOfferResponse> getAllJobOffers();

    @GET("jobOffer/getPostedJob/{id}")
    Call<JobOfferResponse> getPostedJobs(@Path("id") String id);

    @POST("jobOffer/add")
    Call<JobOfferResponse> addJobOffer(@Body JobOffer jobOffer);

    @PUT("jobOffer/update/{id}")
    Call<JobOfferResponse> updateJobOffer(@Path("id") String id, @Body JobOffer jobOffer);

    @DELETE("jobOffer/delete/{id}")
    Call<JobOfferResponse> deleteJobOffer(@Path("id") String id);
}
