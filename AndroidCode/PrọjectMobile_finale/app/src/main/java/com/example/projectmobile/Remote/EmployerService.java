package com.example.projectmobile.Remote;

import com.example.projectmobile.Model.Employer;
import com.example.projectmobile.Model.EmployerResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface EmployerService {
    @POST("employeur/login")
    Call<EmployerResponse> loginEmployer(@Body Employer employer);

    @POST("employeur/register")
    Call<EmployerResponse> registerEmployer(@Body Employer employer);

    @PUT("employeur/update/{id}")
    Call<EmployerResponse> updateEmployer(@Path("id") String id, @Body Employer employer);

    @DELETE("employeur/delete/{id}")
    Call<EmployerResponse> deleteEmployer(@Path("id") String id);
}
