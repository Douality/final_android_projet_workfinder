package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.Employer;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;

public class JobDetailsActivity extends AppCompatActivity {

    JobOffer offer;
    long user_type;
    String user_id;
    TextView tv_title, tv_description, tv_salary, tv_location, tv_exp, tv_req, tv_company_name, tv_company_web, tv_to_time;
    Button btn_apply, btn_candidature;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_job_details);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //init variable
        offer = (JobOffer) getIntent().getSerializableExtra("job_offer");
        tv_title = findViewById(R.id.tv_title_job_details);
        tv_description = findViewById(R.id.tv_description_job_details);
        tv_salary = findViewById(R.id.tv_salary_job_details);
        tv_location = findViewById(R.id.tv_location_job_details);
        tv_exp = findViewById(R.id.tv_exp_job_details);
        tv_req = findViewById(R.id.tv_requirement_job_details);
        btn_apply = findViewById(R.id.btn_submit_job_details);
        btn_candidature = findViewById(R.id.btn_all_candidat_job_details);
        tv_company_name = findViewById(R.id.tv_company_name_job_details);
        tv_company_web = findViewById(R.id.tv_company_web_job_details);
        tv_to_time = findViewById(R.id.tv_time_job_details);
        user_type = ((MyApplication) getApplication()).getUserType();
        user_id = ((MyApplication) getApplication()).getUserId();

        //set data to view
        tv_title.setText(offer.getTitle());
        tv_description.setText(offer.getDescription());
        tv_salary.setText("Up to " + offer.getMax_salary().toString() + "$");
        tv_location.setText(offer.getLocation_city() + ", " + offer.getLocation_country());
        if(offer.getMin_experience() > 1) {
            tv_exp.setText(getString(R.string.bullet) + " At least " + offer.getMin_experience() + " years of experiences");
        } else {
            tv_exp.setText(getString(R.string.bullet) + " At least " + offer.getMin_experience() + " year of experiences");
        }
        tv_req.setText(offer.getRequirement());
        tv_company_name.setText(offer.getEmployer().getCompany_name());

        if(offer.getEmployer().getCompany_web() == null || offer.getEmployer().getCompany_web().equals("")) {
            tv_company_web.setText(offer.getEmployer().getEmail());
        } else {
            tv_company_web.setText(offer.getEmployer().getCompany_web());
        }

        tv_to_time.setText(offer.getEndTime());
        if(user_type == 0) {
            btn_candidature.setVisibility(View.GONE);
        } else if (user_type == 1) {
            btn_apply.setVisibility(View.GONE);

            if(!user_id.equals(offer.getEmployer().getId())) {
                btn_candidature.setVisibility(View.GONE);
            }

        } else {
            btn_apply.setVisibility(View.GONE);
            btn_candidature.setVisibility(View.GONE);
        }


        //set event listener
        btn_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                long user_type = ((MyApplication) getApplication()).getUserType();
                if(user_type == 0) {
                    Intent intent = new Intent(JobDetailsActivity.this, ApplyFormActivity.class);
                    intent.putExtra("offer", offer);
                    startActivity(intent);
                } else {
                    Toast.makeText(JobDetailsActivity.this, "You are not a candidature.", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btn_candidature.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(JobDetailsActivity.this, ViewAppliedCandidatActivity.class);
                intent.putExtra("offer_id", offer.getId());
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}