package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.CandidateResponse;
import com.example.projectmobile.Model.Employer;
import com.example.projectmobile.Model.EmployerResponse;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.CandidateService;
import com.example.projectmobile.Remote.EmployerService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class LoginActivity extends AppCompatActivity {

    CandidateService candidateService;
    EmployerService employerService;
    TextView tv_redirect_register;
    Button btn_login;
    Spinner spin_type_login;
    EditText et_account;
    EditText et_password;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //announce
        try {
            String message = this.getIntent().getExtras().getString("message");
            if(message != null) {
                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
                this.getIntent().removeExtra("message");
            }

        } catch (Exception e) {}

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //init variable
        candidateService = APIUtils.getCandidateService();
        employerService = APIUtils.getEmployerService();
        tv_redirect_register = findViewById(R.id.tv_redirect_register);
        spin_type_login = findViewById(R.id.spin_type_login);
        et_account = findViewById(R.id.et_login_account);
        et_password = findViewById(R.id.et_login_password);
        btn_login = findViewById(R.id.btn_submit_login);

        //event listener
        tv_redirect_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(spin_type_login.getSelectedItemId() == 0) {
                    loginCandidate(new Candidate(et_account.getText().toString(), et_password.getText().toString()));
                } else {
                    loginEmployer(new Employer(et_account.getText().toString(), et_password.getText().toString()));
                }
            }
        });
    }

    private void onLoginSuccess(String user_id, Candidate c, Employer e) {
        //update user type
        ((MyApplication) this.getApplication()).setUserType(spin_type_login.getSelectedItemId());
        ((MyApplication) this.getApplication()).setUserId(user_id);
        ((MyApplication) this.getApplication()).setCandidate_info(c);
        ((MyApplication) this.getApplication()).setEmployer_info(e);
        //redirect
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("message", "Login successfully !");
        startActivity(intent.putExtras(bundle));
    }

    public void loginCandidate(Candidate c){
        Call<CandidateResponse> call = candidateService.loginCandidate(c);
        call.enqueue(new Callback<CandidateResponse>() {
            @Override
            public void onResponse(Call<CandidateResponse> call, Response<CandidateResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 200) {
                        onLoginSuccess(response.body().getCandidate_id(), response.body().getCandidate_info(), new Employer());
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(LoginActivity.this, "Request error.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CandidateResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    public void loginEmployer(Employer e){
        Call<EmployerResponse> call = employerService.loginEmployer(e);
        call.enqueue(new Callback<EmployerResponse>() {
            @Override
            public void onResponse(Call<EmployerResponse> call, Response<EmployerResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 200) {
                        onLoginSuccess(response.body().getEmployer_id(), new Candidate(), response.body().getEmployer_info());
                    } else {
                        Toast.makeText(LoginActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(LoginActivity.this, "Request error.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EmployerResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}