package com.example.projectmobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.projectmobile.Activity.JobDetailsActivity;
import com.example.projectmobile.Activity.SubscriptionActivity;
import com.example.projectmobile.Model.Abonnement;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.R;

import java.util.List;

public class SubscriptionAdapter extends ArrayAdapter<Abonnement> {
    private Context context;
    private List<Abonnement> abonnements;
    public String selected_id ="";
    public SubscriptionAdapter(@NonNull Context context, int resource, @NonNull List<Abonnement> objects) {
        super(context, resource, objects);
        this.context = context;
        this.abonnements = objects;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_subscription, parent, false);

        //init variable
        TextView name = rowView.findViewById(R.id.tv_name_subscription);
        TextView description = rowView.findViewById(R.id.tv_description_subscription);
        TextView price = rowView.findViewById(R.id.tv_price_subscription);
        LinearLayout ll_subscription = rowView.findViewById(R.id.ll_subscription_child);

        //set data
        name.setText(abonnements.get(pos).getName());
        description.setText(abonnements.get(pos).getDescription());
        price.setText(abonnements.get(pos).getPrice() + "$");

        //set event listener
        ll_subscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                for(int i = 0; i < abonnements.size(); i ++) {
                    parent.getChildAt(i).findViewById(R.id.ll_subscription_child).setBackground(context.getDrawable(R.drawable.normal_stroke));
                }
                ll_subscription.setBackground(context.getDrawable(R.drawable.select_stroke));
                selected_id = abonnements.get(pos).getId();
            }
        });

        return rowView;
    }
}
