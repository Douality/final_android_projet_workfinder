package com.example.projectmobile.Activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;
import android.widget.Toast;

import com.example.projectmobile.Adapter.MainPagerAdapter;
import com.example.projectmobile.Fragment.HomeFragment;
import com.example.projectmobile.Fragment.JobFragment;
import com.example.projectmobile.Fragment.ProfileFragment;
import com.example.projectmobile.R;
import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {

    ViewPager2 viewPager;
    TabLayout tabLayout;
    private int[] tabIcons = {
            R.drawable.ic_home_page,
            R.drawable.ic_job_page,
            R.drawable.ic_profile_page
    };

    private String[] tabTexts = { "Home", "Jobs", "Profile" };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        //announce
        try {
            String message = this.getIntent().getExtras().getString("message");
            if(message != null) {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_SHORT).show();
                this.getIntent().removeExtra("message");
            }

        } catch (Exception e) {}

        //init variable
        viewPager = findViewById(R.id.main_pager);
        tabLayout = findViewById(R.id.main_tab_layout);

        MainPagerAdapter mainPagerAdapter = new MainPagerAdapter(getSupportFragmentManager(), getLifecycle());
        mainPagerAdapter.addFragment(new HomeFragment());
        mainPagerAdapter.addFragment(new JobFragment());
        mainPagerAdapter.addFragment(new ProfileFragment());
        viewPager.setOrientation(ViewPager2.ORIENTATION_HORIZONTAL);
        viewPager.setAdapter(mainPagerAdapter);

        new TabLayoutMediator(tabLayout, viewPager, (tab, position) -> {
            tab.setIcon(tabIcons[position]);

            //tab.setText(tabTexts[position]);
        }).attach();

        //tab
        try {
            Integer tab = this.getIntent().getExtras().getInt("tab");
            if(tab != null) {
                viewPager.setCurrentItem(tab);
                this.getIntent().removeExtra("tab");
            }

        } catch (Exception e) {}

    }
}