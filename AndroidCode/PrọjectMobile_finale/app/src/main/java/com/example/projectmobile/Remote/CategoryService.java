package com.example.projectmobile.Remote;

import com.example.projectmobile.Model.Category;
import com.example.projectmobile.Model.CategoryResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CategoryService {
    @GET("category/getAll")
    Call<CategoryResponse> getCategories();

    @POST("category/add")
    Call<CategoryResponse> addCategory(@Body Category category);

    @PUT("category/update/{id}")
    Call<Category> updateCategory(@Path("id") String id, @Body Category category);

    @DELETE("category/delete/{id}")
    Call<CategoryResponse> deleteCategory(@Path("id") String id);
}

