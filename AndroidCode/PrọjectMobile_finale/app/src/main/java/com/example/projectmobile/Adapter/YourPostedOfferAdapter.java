package com.example.projectmobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.projectmobile.Activity.JobDetailsActivity;
import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.R;

import java.util.List;

public class YourPostedOfferAdapter extends ArrayAdapter<JobOffer> {
    private Context context;
    private List<JobOffer> jobOffers;

    public YourPostedOfferAdapter(@NonNull Context context, int resource, @NonNull List<JobOffer> objects) {
        super(context, resource, objects);
        this.jobOffers = objects;
        this.context = context;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_your_job, parent, false);

        //init variable
        TextView tv_title = rowView.findViewById(R.id.tv_name_your_job);
        TextView tv_status = rowView.findViewById(R.id.tv_status_your_job);
        TextView tv_date = rowView.findViewById(R.id.tv_date_your_job);
        LinearLayout ll_your_job = rowView.findViewById(R.id.ll_your_job);

        //set data
        tv_title.setText(jobOffers.get(pos).getTitle());
        boolean status = jobOffers.get(pos).isOutDate();
        if(status == false) {
            tv_status.setText("ongoing");
            tv_status.setTextColor(context.getColor(R.color.green_light));
        } else  {
            tv_status.setText("outdate");
            tv_status.setTextColor(context.getColor(R.color.red));
        }

        tv_date.setText(jobOffers.get(pos).getCreateDate());

        //set listener
        ll_your_job.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, JobDetailsActivity.class);
                intent.putExtra("job_offer", jobOffers.get(pos));
                context.startActivity(intent);
            }
        });
        return rowView;
    }
}
