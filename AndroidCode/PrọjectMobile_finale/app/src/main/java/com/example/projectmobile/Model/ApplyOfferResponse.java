package com.example.projectmobile.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApplyOfferResponse {
    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("list")
    private List<ApplyOffer> applyOffers;

    public ApplyOfferResponse() {
    }

    public ApplyOfferResponse(int code, String message, List<ApplyOffer> applyOffers) {
        this.code = code;
        this.message = message;
        this.applyOffers = applyOffers;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ApplyOffer> getApplyOffers() {
        return applyOffers;
    }

    public void setApplyOffers(List<ApplyOffer> applyOffers) {
        this.applyOffers = applyOffers;
    }
}
