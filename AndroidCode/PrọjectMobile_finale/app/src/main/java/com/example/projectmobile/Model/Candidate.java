package com.example.projectmobile.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Candidate implements Serializable {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("username")
    @Expose
    private String username;

    @SerializedName("password")
    @Expose
    private String password;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("phone")
    @Expose
    private String phone;

    @SerializedName("fullname")
    @Expose
    private String full_name;

    @SerializedName("location_city")
    @Expose
    private String location_city;

    @SerializedName("location_country")
    @Expose
    private String location_country;

    @SerializedName("link_cv")
    @Expose
    private String link_cv;

    public Candidate() {
    }

    public Candidate(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public Candidate(String username, String password, String email, String phone, String full_name) {
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.full_name = full_name;
    }

    public Candidate(String id, String username, String password, String email, String phone, String full_name, String location_city, String location_country, String link_cv) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.email = email;
        this.phone = phone;
        this.full_name = full_name;
        this.location_city = location_city;
        this.location_country = location_country;
        this.link_cv = link_cv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getLocation_city() {
        return location_city;
    }

    public void setLocation_city(String location_city) {
        this.location_city = location_city;
    }

    public String getLocation_country() {
        return location_country;
    }

    public void setLocation_country(String location_country) {
        this.location_country = location_country;
    }

    public String getLink_cv() {
        return link_cv;
    }

    public void setLink_cv(String link_cv) {
        this.link_cv = link_cv;
    }
}
