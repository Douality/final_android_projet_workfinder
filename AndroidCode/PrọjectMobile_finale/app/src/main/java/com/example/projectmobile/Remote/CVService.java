package com.example.projectmobile.Remote;

import com.example.projectmobile.Model.CV;
import com.example.projectmobile.Model.CVResponse;
import com.example.projectmobile.Model.Category;
import com.example.projectmobile.Model.CategoryResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface CVService {
    @GET("cv/getAll/{id}")
    Call<CVResponse> getCVs(@Path("id") String id);

    @GET("cv/find/{id}")
    Call<CVResponse> getACv(@Path("id") String id);

    @POST("cv/add")
    Call<CVResponse> addCV(@Body CV cv);

    @PUT("cv/update/{id}")
    Call<CVResponse> updateCV(@Path("id") String id, @Body CV cv);

    @DELETE("cv/delete/{id}")
    Call<CVResponse> deleteCV(@Path("id") String id);
}
