package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.projectmobile.Adapter.ListCvAdapter;
import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.Model.ApplyOfferResponse;
import com.example.projectmobile.Model.CV;
import com.example.projectmobile.Model.CVResponse;
import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.Category;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.ApplyOfferService;
import com.example.projectmobile.Remote.CVService;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApplyFormActivity extends AppCompatActivity {

    ApplyOfferService applyOfferService;
    CVService cvService;
    Spinner spinner_cv;
    EditText et_motivation, et_question;
    Button btn_apply;
    List<CV> cvs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apply_form);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //init variable
        applyOfferService = APIUtils.getApplyOfferService();
        cvService = APIUtils.getCVService();
        spinner_cv = findViewById(R.id.spin_cv_apply_job);
        et_motivation = findViewById(R.id.et_motivation_apply_job);
        et_question = findViewById(R.id.et_question_apply_job);
        btn_apply = findViewById(R.id.btn_submit_apply_job);
        GetAllCVs();

        //add event listener
        btn_apply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hasEmptyFields() == false) {
                    ApplyJob();
                } else {
                    Toast.makeText(ApplyFormActivity.this, "Please fill all required fields.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    void ApplyJob() {
        Candidate candidate = ((MyApplication)getApplication()).getCandidate_info();
        JobOffer offer = (JobOffer) getIntent().getSerializableExtra("offer");
        CV cv = cvs.get((int) spinner_cv.getSelectedItemId());
        String motivation = et_motivation.getText().toString();
        String question = et_question.getText().toString();

        ApplyOffer applyOffer = new ApplyOffer(candidate, offer, cv, motivation, question);

        Call<ApplyOfferResponse> call = applyOfferService.applyJobOffer(applyOffer);
        call.enqueue(new Callback<ApplyOfferResponse>() {
            @Override
            public void onResponse(Call<ApplyOfferResponse> call, Response<ApplyOfferResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 200) {
                        onApplySuccess();
                    } else {
                        Toast.makeText(ApplyFormActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(ApplyFormActivity.this, "Job applied fail!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApplyOfferResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    private void onApplySuccess() {
        Intent intent = new Intent(ApplyFormActivity.this, YourJobOfferActivity.class);
        intent.putExtra("message", "Apply job successfully!");
        startActivity(intent);
    }

    void GetAllCVs() {
        String user_id = ((MyApplication)getApplication()).getUserId();
        Call<CVResponse> call = cvService.getCVs(user_id);
        call.enqueue(new Callback<CVResponse>() {
            @Override
            public void onResponse(Call<CVResponse> call, Response<CVResponse> response) {
                if(response.isSuccessful()){
                    cvs = response.body().getCvs();
                    if(cvs.size() == 0) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(ApplyFormActivity.this);
                        builder.setMessage("You need to create a cv first.").setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                finish();
                            }
                        }).show();

                    } else {
                        cvs.add(new CV("", "Select one of your cvs", "", "", "", ""));
                        Collections.reverse(cvs);

                        //update spinner
                        ArrayAdapter<CV> cvAdapter = new ArrayAdapter<>(ApplyFormActivity.this,
                                android.R.layout.simple_spinner_item, cvs);
                        cvAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinner_cv.setAdapter(cvAdapter);
                    }
                }
            }

            @Override
            public void onFailure(Call<CVResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    boolean hasEmptyFields() {
        if(spinner_cv.getSelectedItemId() == 0) return true;
        if(et_motivation.getText().toString().equals("")) return true;

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}