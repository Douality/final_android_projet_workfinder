package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.Model.ApplyOfferResponse;
import com.example.projectmobile.Model.CV;
import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.ApplyOfferService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResponseCandidatureActivity extends AppCompatActivity {

    ApplyOfferService applyOfferService;
    EditText et_response;
    Spinner spinner_status;
    Button btn_response;
    ApplyOffer candidature_offer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_response_candidature);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //init variable
        applyOfferService = APIUtils.getApplyOfferService();
        et_response = findViewById(R.id.et_response_candidature);
        spinner_status = findViewById(R.id.spin_response_status);
        btn_response = findViewById(R.id.btn_submit_response_candidature);
        candidature_offer = (ApplyOffer) getIntent().getSerializableExtra("candidature_offer");

        //set data
        spinner_status.setSelection(candidature_offer.getApply_status());
        et_response.setText(candidature_offer.getResponse());

        //event listener
        btn_response.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResponseCandidature();
            }

        });
    }

    private void ResponseCandidature() {

        candidature_offer.setApply_status((int)spinner_status.getSelectedItemId());
        candidature_offer.setResponse(et_response.getText().toString());

        Call<ApplyOfferResponse> call = applyOfferService.responseCandidature(candidature_offer.getId(), candidature_offer);
        call.enqueue(new Callback<ApplyOfferResponse>() {
            @Override
            public void onResponse(Call<ApplyOfferResponse> call, Response<ApplyOfferResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 200) {
                        onResponseSuccess();
                    } else {
                        Toast.makeText(ResponseCandidatureActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(ResponseCandidatureActivity.this, "Response fail!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ApplyOfferResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    private void onResponseSuccess() {
        Intent intent = new Intent(ResponseCandidatureActivity.this, ViewAppliedCandidatActivity.class);
        intent.putExtra("offer_id", candidature_offer.getOffer().getId());
        intent.putExtra("message", "Response candidature successfully!");
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}