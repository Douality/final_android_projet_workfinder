package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.CandidateResponse;
import com.example.projectmobile.Model.Employer;
import com.example.projectmobile.Model.EmployerResponse;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.CandidateService;
import com.example.projectmobile.Remote.EmployerService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity {

    CandidateService candidateService;
    EmployerService employerService;
    Button btn_submit;
    EditText et_full_name;
    EditText et_account;
    EditText et_password;
    EditText et_confirm_password;
    EditText et_email;
    EditText et_phone;
    Spinner spin_type_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        //init variable
        candidateService = APIUtils.getCandidateService();
        employerService = APIUtils.getEmployerService();
        btn_submit = findViewById(R.id.btn_submit_register);
        et_full_name = findViewById(R.id.et_register_full_name);
        et_account = findViewById(R.id.et_register_account);
        et_password = findViewById(R.id.et_register_password);
        et_confirm_password = findViewById(R.id.et_register_password_confirm);
        et_email = findViewById(R.id.et_register_email);
        et_phone = findViewById(R.id.et_register_phone);
        spin_type_register = findViewById(R.id.spin_type_register);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //set event listener
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hasEmptyFields() == true) {
                    Toast.makeText(RegisterActivity.this, "Please fill all required fields.", Toast.LENGTH_SHORT).show();
                    return;
                }

                if(!et_password.getText().toString().equals(et_confirm_password.getText().toString())) {
                    Toast.makeText(RegisterActivity.this, "Password confirm is not matched.", Toast.LENGTH_SHORT).show();
                    return;
                }

                registerAccount();
            }
        });
    }

    private boolean hasEmptyFields() {
        if(et_email.getText().toString().equals("")) { return true; }
        if(et_full_name.getText().toString().equals("")) { return true; }
        if(et_account.getText().toString().equals("")) { return true; }
        if(et_password.getText().toString().equals("")) { return true; }
        if(et_phone.getText().toString().equals("")) { return true; }
        if(et_confirm_password.getText().toString().equals("")) { return true; }

        return false;
    }

    private void registerAccount() {
        if(spin_type_register.getSelectedItemId() == 0) {
            registerCandidate(new Candidate(
                    et_account.getText().toString(),
                    et_password.getText().toString(),
                    et_email.getText().toString(),
                    et_phone.getText().toString(),
                    et_full_name.getText().toString()
            ));
        } else {
            registerEmployer(new Employer(
                    et_account.getText().toString(),
                    et_password.getText().toString(),
                    et_email.getText().toString(),
                    et_phone.getText().toString(),
                    et_full_name.getText().toString()
            ));
        }
    }

    private void onRegisterSuccess() {
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("message", "Register account successfully !");
        startActivity(intent.putExtras(bundle));
    }

    private void registerCandidate(Candidate c) {
        Call<CandidateResponse> call = candidateService.registerCandidate(c);
        call.enqueue(new Callback<CandidateResponse>() {
            @Override
            public void onResponse(Call<CandidateResponse> call, Response<CandidateResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 200) {
                        onRegisterSuccess();
                    } else {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(RegisterActivity.this, "Request error.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CandidateResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    private void registerEmployer(Employer e) {
        Call<EmployerResponse> call = employerService.registerEmployer(e);
        call.enqueue(new Callback<EmployerResponse>() {
            @Override
            public void onResponse(Call<EmployerResponse> call, Response<EmployerResponse> response) {
                if(response.isSuccessful()){
                    if(response.body().getCode() == 200) {
                        onRegisterSuccess();
                    } else {
                        Toast.makeText(RegisterActivity.this, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(RegisterActivity.this, "Request error.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<EmployerResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}