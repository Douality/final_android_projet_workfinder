package com.example.projectmobile.Remote;

import com.example.projectmobile.Model.ApplyOffer;

public class APIUtils {
    private APIUtils(){
    };

    public static final String API_URL = "http://192.168.0.34:3000/";

    public static CategoryService getCategoryService(){
        return RetrofitClient.getClient(API_URL).create(CategoryService.class);
    }

    public static CandidateService getCandidateService(){
        return RetrofitClient.getClient(API_URL).create(CandidateService.class);
    }

    public static EmployerService getEmployerService(){
        return RetrofitClient.getClient(API_URL).create(EmployerService.class);
    }

    public static JobOfferService getJobOfferService(){
        return RetrofitClient.getClient(API_URL).create(JobOfferService.class);
    }

    public static CVService getCVService() {
        return RetrofitClient.getClient(API_URL).create(CVService.class);
    }

    public static ApplyOfferService getApplyOfferService() {
        return RetrofitClient.getClient(API_URL).create(ApplyOfferService.class);
    }

    public static AbonnementService getAbonnementService() {
        return RetrofitClient.getClient(API_URL).create(AbonnementService.class);
    }
}
