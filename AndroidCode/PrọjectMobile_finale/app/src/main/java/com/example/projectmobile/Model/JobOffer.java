package com.example.projectmobile.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class JobOffer implements Serializable {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("employeur_id")
    @Expose
    private Employer employer;

    @SerializedName("category_id")
    @Expose
    private Category category;

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("location_city")
    @Expose
    private String location_city;

    @SerializedName("location_country")
    @Expose
    private String location_country;

    @SerializedName("max_salary")
    @Expose
    private Long max_salary;

    @SerializedName("min_experience")
    @Expose
    private Long min_experience;

    @SerializedName("requirement")
    @Expose
    private String requirement;

    @SerializedName("fromTime")
    @Expose
    private Long fromTime;

    @SerializedName("toTime")
    @Expose
    private Long toTime;

    @SerializedName("duration")
    @Expose
    private Long duration;


    public JobOffer() {}

    public JobOffer(Employer employer, Category category, String title, String description, String location_city, String location_country, Long max_salary, Long min_experience, String requirement, Long duration) {
        this.employer = employer;
        this.category = category;
        this.title = title;
        this.description = description;
        this.location_city = location_city;
        this.location_country = location_country;
        this.max_salary = max_salary;
        this.min_experience = min_experience;
        this.requirement = requirement;
        this.duration = duration;
    }

    public JobOffer(String id, Employer employer, Category category, String title, String description, String location_city, String location_country, Long max_salary, Long min_experience, String requirement, Long fromTime, Long toTime, Long duration) {
        this.id = id;
        this.employer = employer;
        this.category = category;
        this.title = title;
        this.description = description;
        this.location_city = location_city;
        this.location_country = location_country;
        this.max_salary = max_salary;
        this.min_experience = min_experience;
        this.requirement = requirement;
        this.fromTime = fromTime;
        this.toTime = toTime;
        this.duration = duration;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Employer getEmployer() {
        return employer;
    }

    public void setEmployer(Employer employer) {
        this.employer = employer;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLocation_city() {
        return location_city;
    }

    public void setLocation_city(String location_city) {
        this.location_city = location_city;
    }

    public String getLocation_country() {
        return location_country;
    }

    public void setLocation_country(String location_country) {
        this.location_country = location_country;
    }

    public Long getMax_salary() {
        return max_salary;
    }

    public void setMax_salary(Long max_salary) {
        this.max_salary = max_salary;
    }

    public Long getMin_experience() {
        return min_experience;
    }

    public void setMin_experience(Long min_experience) {
        this.min_experience = min_experience;
    }

    public String getRequirement() {
        return requirement;
    }

    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    public Long getFromTime() {
        return fromTime;
    }

    public void setFromTime(Long fromTime) {
        this.fromTime = fromTime;
    }

    public Long getToTime() {
        return toTime;
    }

    public void setToTime(Long toTime) {
        this.toTime = toTime;
    }

    public Long getDuration() {
        return duration;
    }

    public void setDuration(Long duration) {
        this.duration = duration;
    }

    public String getEndTime() {
        Date date = new Date(toTime);

        //date to string
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy 'at' hh:mm");
        String dateTime = dateFormat.format(date);

        return dateTime;
    }

    public String getCreateDate() {
        Date date = new Date(fromTime);

        //date to string
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateTime = dateFormat.format(date);

        return dateTime;
    }

    public boolean isOutDate() {
        Date date = new Date();
        if(date.getTime() > toTime) {
            return true;
        }

        return false;
    }
}
