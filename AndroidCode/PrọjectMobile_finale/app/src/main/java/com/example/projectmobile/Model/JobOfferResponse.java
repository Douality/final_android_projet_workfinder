package com.example.projectmobile.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class JobOfferResponse {
    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("list")
    private List<JobOffer> jobOffers;

    public JobOfferResponse() {}

    public JobOfferResponse(int code, String message, List<JobOffer> jobOffers) {
        this.code = code;
        this.message = message;
        this.jobOffers = jobOffers;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<JobOffer> getJobOffers() {
        return jobOffers;
    }

    public void setJobOffers(List<JobOffer> jobOffers) {
        this.jobOffers = jobOffers;
    }
}
