package com.example.projectmobile.Adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.projectmobile.Activity.JobDetailsActivity;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.R;

import java.util.List;

public class JobOfferAdapter extends ArrayAdapter<JobOffer> {
    private Context context;
    private List<JobOffer> jobOffers;

    public JobOfferAdapter(@NonNull Context context, int resource, @NonNull List<JobOffer> jobOffers) {
        super(context, resource, jobOffers);
        this.context = context;
        this.jobOffers = jobOffers;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_job, parent, false);

        //init variable
        TextView title = rowView.findViewById(R.id.tv_list_job_title);
        TextView location = rowView.findViewById(R.id.tv_list_job_address);
        TextView salary = rowView.findViewById(R.id.tv_list_job_salary);
        TextView exp = rowView.findViewById(R.id.tv_list_job_exp);
        LinearLayout ll_job_child = rowView.findViewById(R.id.ll_job_child);

        //set data
        title.setText(jobOffers.get(pos).getTitle());
        location.setText(jobOffers.get(pos).getLocation_city() + ", " + jobOffers.get(pos).getLocation_country());
        salary.setText("Up to " + jobOffers.get(pos).getMax_salary().toString() + "$");

        if(jobOffers.get(pos).getMin_experience() > 1) {
            exp.setText(jobOffers.get(pos).getMin_experience().toString() + " years experiences");
        } else {
            exp.setText(jobOffers.get(pos).getMin_experience().toString() + " year experiences");
        }

        //set event listener
        ll_job_child.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(context, JobDetailsActivity.class);
                intent.putExtra("job_offer", jobOffers.get(pos));
                context.startActivity(intent);
            }
        });

        return rowView;
    }
}
