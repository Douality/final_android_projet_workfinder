package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.example.projectmobile.Adapter.JobOfferAdapter;
import com.example.projectmobile.Adapter.ListCvAdapter;
import com.example.projectmobile.Model.CV;
import com.example.projectmobile.Model.CVResponse;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.Model.JobOfferResponse;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.CVService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ViewAllCVActivity extends AppCompatActivity {

    CVService cvService;
    ListView lv_all_cv;
    Button btnAddCV;
    List<CV> cvs = new ArrayList<CV>();;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_all_cvactivity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        //announce
        try {
            String message = this.getIntent().getExtras().getString("message");
            if(message != null) {
                Toast.makeText(ViewAllCVActivity.this, message, Toast.LENGTH_SHORT).show();
                this.getIntent().removeExtra("message");
            }

        } catch (Exception e) {}

        //init variable
        btnAddCV = findViewById(R.id.btn_add_cv);
        lv_all_cv = findViewById(R.id.lv_all_cv);
        cvService = APIUtils.getCVService();
        GetAllCVs();

        //add event listener
        btnAddCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ViewAllCVActivity.this, CreateCvActivity.class);
                startActivity(intent);
            }
        });
    }

    void GetAllCVs() {
        String user_id = ((MyApplication)getApplication()).getUserId();
        Call<CVResponse> call = cvService.getCVs(user_id);
        call.enqueue(new Callback<CVResponse>() {
            @Override
            public void onResponse(Call<CVResponse> call, Response<CVResponse> response) {
                if(response.isSuccessful()){
                    cvs = response.body().getCvs();
                    Collections.reverse(cvs);
                    lv_all_cv.setAdapter(new ListCvAdapter(ViewAllCVActivity.this, R.layout.list_cv, cvs));
                }
            }

            @Override
            public void onFailure(Call<CVResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}