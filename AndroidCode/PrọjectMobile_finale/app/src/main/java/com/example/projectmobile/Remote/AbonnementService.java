package com.example.projectmobile.Remote;

import com.example.projectmobile.Model.AbonnementResponse;
import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.Model.ApplyOfferResponse;
import com.example.projectmobile.Model.UserAbonnement;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;

public interface AbonnementService {
    @GET("abonnement/getAll")
    Call<AbonnementResponse> getAllAbonnement();

    @POST("abonnement/make")
    Call<AbonnementResponse> makeAbonnement(@Body UserAbonnement userAbonnement);

    @GET("abonnement/check/{id}")
    Call<AbonnementResponse> checkAbonnement(@Path("id") String id);
}
