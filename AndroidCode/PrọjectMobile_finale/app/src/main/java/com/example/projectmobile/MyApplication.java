package com.example.projectmobile;

import android.app.Application;

import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.Model.Category;
import com.example.projectmobile.Model.Employer;

import java.util.List;

public class MyApplication extends Application {
    private long userType = -1; //anonymous = -1 , candidat = 0, employer = 1
    private String userId = "";

    private Employer employer_info;
    private Candidate candidate_info;

    public Employer getEmployer_info() {
        return employer_info;
    }

    public void setEmployer_info(Employer employer_info) {
        this.employer_info = employer_info;
    }

    public Candidate getCandidate_info() {
        return candidate_info;
    }

    public void setCandidate_info(Candidate candidate_info) {
        this.candidate_info = candidate_info;
    }

    public long getUserType() {
        return userType;
    }

    public void setUserType(long userType) {
        this.userType = userType;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
