package com.example.projectmobile.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ApplyOffer implements Serializable {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("candidat_id")
    @Expose
    private Candidate candidate;

    @SerializedName("offer_id")
    @Expose
    private JobOffer offer;

    @SerializedName("cv_id")
    @Expose
    private CV cv;

    @SerializedName("letter_motivation")
    @Expose
    private String letter_motivation;

    @SerializedName("autre_question")
    @Expose
    private String autre_question;

    @SerializedName("apply_status")
    @Expose
    private Integer apply_status;

    @SerializedName("response")
    @Expose
    private String response;

    @SerializedName("createdAt")
    @Expose
    private String createdAt;


    public ApplyOffer() {}

    public ApplyOffer(String id, Candidate candidate, JobOffer offer, CV cv, String letter_motivation, String autre_question, Integer apply_status, String response) {
        this.id = id;
        this.candidate = candidate;
        this.offer = offer;
        this.cv = cv;
        this.letter_motivation = letter_motivation;
        this.autre_question = autre_question;
        this.apply_status = apply_status;
        this.response = response;
    }

    public ApplyOffer(Candidate candidate, JobOffer offer, CV cv, String letter_motivation, String autre_question) {
        this.candidate = candidate;
        this.offer = offer;
        this.cv = cv;
        this.letter_motivation = letter_motivation;
        this.autre_question = autre_question;
        this.apply_status = 0;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Candidate getCandidat() {
        return candidate;
    }

    public void setCandidat(Candidate candidate) {
        this.candidate = candidate;
    }

    public JobOffer getOffer() {
        return offer;
    }

    public void setOffer(JobOffer offer) {
        this.offer = offer;
    }

    public CV getCv() {
        return cv;
    }

    public void setCv(CV cv) {
        this.cv = cv;
    }

    public String getLetter_motivation() {
        return letter_motivation;
    }

    public void setLetter_motivation(String letter_motivation) {
        this.letter_motivation = letter_motivation;
    }

    public String getAutre_question() {
        return autre_question;
    }

    public void setAutre_question(String autre_question) {
        this.autre_question = autre_question;
    }

    public Integer getApply_status() {
        return apply_status;
    }

    public void setApply_status(Integer apply_status) {
        this.apply_status = apply_status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getCreateDate() {
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        try {
            date = format.parse(createdAt);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        //date to string
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        String dateTime = dateFormat.format(date);

        return dateTime;
    }
}
