package com.example.projectmobile.Model;

import androidx.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class CV implements Serializable {
    @SerializedName("_id")
    @Expose
    private String id;

    @SerializedName("candidat_id")
    @Expose
    private String candidat_id;

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("study_progress")
    @Expose
    private String study_progress;

    @SerializedName("skills")
    @Expose
    private String skills;

    @SerializedName("experiences")
    @Expose
    private String experiences;

    @SerializedName("others")
    @Expose
    private String others;

    public CV() {}

    public CV(String id, String candidat_id, String name, String study_progress, String skills, String experiences, String others) {
        this.id = id;
        this.candidat_id = candidat_id;
        this.name = name;
        this.study_progress = study_progress;
        this.skills = skills;
        this.experiences = experiences;
        this.others = others;
    }

    public CV(String candidat_id, String name, String study_progress, String skills, String experiences, String others) {
        this.candidat_id = candidat_id;
        this.name = name;
        this.study_progress = study_progress;
        this.skills = skills;
        this.experiences = experiences;
        this.others = others;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCandidate() {
        return candidat_id;
    }

    public void setCandidate(String candidat_id) {
        this.candidat_id = candidat_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getStudy_progress() {
        return study_progress;
    }

    public void setStudy_progress(String study_progress) {
        this.study_progress = study_progress;
    }

    public String getSkills() {
        return skills;
    }

    public void setSkills(String skills) {
        this.skills = skills;
    }

    public String getExperiences() {
        return experiences;
    }

    public void setExperiences(String experiences) {
        this.experiences = experiences;
    }

    public String getOthers() {
        return others;
    }

    public void setOthers(String others) {
        this.others = others;
    }

    @NonNull
    @Override
    public String toString() {
        return name;
    }
}
