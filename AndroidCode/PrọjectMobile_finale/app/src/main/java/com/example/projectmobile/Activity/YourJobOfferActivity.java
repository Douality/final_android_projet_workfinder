package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.ListView;

import com.example.projectmobile.Adapter.ListCvAdapter;
import com.example.projectmobile.Adapter.YourApplyOfferAdapter;
import com.example.projectmobile.Adapter.YourPostedOfferAdapter;
import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.Model.ApplyOfferResponse;
import com.example.projectmobile.Model.CVResponse;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.Model.JobOfferResponse;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.ApplyOfferService;
import com.example.projectmobile.Remote.JobOfferService;

import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YourJobOfferActivity extends AppCompatActivity {

    long userType;
    ApplyOfferService applyOfferService;
    JobOfferService jobOfferService;
    ListView lv_your_job;
    List<ApplyOffer> candidatOffers;
    List<JobOffer> employerOffers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_your_job_offer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //init variable
        applyOfferService = APIUtils.getApplyOfferService();
        jobOfferService = APIUtils.getJobOfferService();
        lv_your_job = findViewById(R.id.lv_all_your_job);
        userType = ((MyApplication)getApplication()).getUserType();

        if(userType == 0) {
            GetCandidatJob();
        } else if(userType == 1) {
            GetEmployerJob();
        }
    }

    void GetEmployerJob() {
        String user_id = ((MyApplication)getApplication()).getUserId();
        Call<JobOfferResponse> call = jobOfferService.getPostedJobs(user_id);
        call.enqueue(new Callback<JobOfferResponse>() {
            @Override
            public void onResponse(Call<JobOfferResponse> call, Response<JobOfferResponse> response) {
                if(response.isSuccessful()){
                    employerOffers = response.body().getJobOffers();
                    Collections.reverse(employerOffers);
                    lv_your_job.setAdapter(new YourPostedOfferAdapter(YourJobOfferActivity.this, R.layout.list_cv, employerOffers));
                }
            }

            @Override
            public void onFailure(Call<JobOfferResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    void GetCandidatJob() {
        String user_id = ((MyApplication)getApplication()).getUserId();
        Call<ApplyOfferResponse> call = applyOfferService.getAppliedOffer(user_id);
        call.enqueue(new Callback<ApplyOfferResponse>() {
            @Override
            public void onResponse(Call<ApplyOfferResponse> call, Response<ApplyOfferResponse> response) {
                if(response.isSuccessful()){
                    candidatOffers = response.body().getApplyOffers();
                    Collections.reverse(candidatOffers);
                    lv_your_job.setAdapter(new YourApplyOfferAdapter(YourJobOfferActivity.this, R.layout.list_cv, candidatOffers));
                }
            }

            @Override
            public void onFailure(Call<ApplyOfferResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}