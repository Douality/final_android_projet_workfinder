package com.example.projectmobile.Model;

import com.google.gson.annotations.SerializedName;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AbonnementResponse {
    @SerializedName("code")
    private int code;

    @SerializedName("message")
    private String message;

    @SerializedName("list")
    private List<Abonnement> abonnements;

    @SerializedName("abonnement_info")
    private Abonnement abonnement;

    @SerializedName("out_date_time")
    private long out_date_time;


    public AbonnementResponse() {
    }

    public AbonnementResponse(int code, String message, List<Abonnement> abonnements) {
        this.code = code;
        this.message = message;
        this.abonnements = abonnements;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Abonnement> getAbonnements() {
        return abonnements;
    }

    public void setAbonnements(List<Abonnement> abonnements) {
        this.abonnements = abonnements;
    }

    public Abonnement getAbonnement() {
        return abonnement;
    }

    public void setAbonnement(Abonnement abonnement) {
        this.abonnement = abonnement;
    }

    public boolean isOutDate() {
        Date date = new Date();
        if(date.getTime() > out_date_time) {
            return true;
        }

        return  false;
    }

    public String getOutDateTime() {
        Date date = new Date(out_date_time);
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy 'at' hh:mm");
        return format.format(date);
    }

    public void setOut_date_time(long out_date_time) {
        this.out_date_time = out_date_time;
    }


}
