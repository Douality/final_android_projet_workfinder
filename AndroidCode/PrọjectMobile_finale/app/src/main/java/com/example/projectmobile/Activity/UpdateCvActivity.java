package com.example.projectmobile.Activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.projectmobile.Model.CV;
import com.example.projectmobile.Model.CVResponse;
import com.example.projectmobile.Model.Candidate;
import com.example.projectmobile.MyApplication;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.CVService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateCvActivity extends AppCompatActivity {

    CV currentCV;
    CVService cvService;
    EditText et_title, et_learningData, et_experience, et_skill, et_other;
    Button btn_updateCV;
    Integer rql_learning = 0, rql_exp = 0, rql_skill = 0, rql_other = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_cv);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        //init variable
        cvService = APIUtils.getCVService();
        et_title = findViewById(R.id.et_title_update_cv);
        et_learningData = findViewById(R.id.et_study_update_cv);
        et_experience = findViewById(R.id.et_exp_update_cv);
        et_skill = findViewById(R.id.et_skill_update_cv);
        et_other = findViewById(R.id.et_other_update_cv);
        btn_updateCV = findViewById(R.id.btn_submit_update_cv);

        //update UI
        currentCV = (CV) getIntent().getSerializableExtra("cv_info");
        et_title.setText(currentCV.getName());
        et_learningData.setText(currentCV.getStudy_progress());
        et_experience.setText(currentCV.getExperiences());
        et_skill.setText(currentCV.getSkills());
        et_other.setText(currentCV.getOthers());

        //event listener
        btn_updateCV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(hasEmptyFields() == false) {
                    updateCV();
                } else {
                    Toast.makeText(UpdateCvActivity.this, "Please fill all required fields.", Toast.LENGTH_SHORT).show();
                }
            }
        });

        et_learningData.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                // Add bullet points when the user starts a new line

                if(rql_learning < charSequence.length()) {
                    String[] lines = charSequence.toString().split("\n");
                    for (int i = 0; i < lines.length; i++) {
                        if (!lines[i].startsWith(getString(R.string.bullet))) {
                            lines[i] = getString(R.string.bullet) + " " + lines[i];
                        }
                    }

                    String newText = TextUtils.join("\n", lines);
                    if(charSequence.charAt(charSequence.length() - 1) == '\n') {
                        newText += "\n" + getString(R.string.bullet) + " ";
                    }

                    if (!newText.equals(et_learningData.getText().toString())) {
                        // The text has actually changed, so we can safely set it
                        et_learningData.setText(newText);
                        et_learningData.setSelection(newText.length());
                    }

                }
                rql_learning = charSequence.length();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_experience.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                // Add bullet points when the user starts a new line

                if(rql_exp < charSequence.length()) {
                    String[] lines = charSequence.toString().split("\n");
                    for (int i = 0; i < lines.length; i++) {
                        if (!lines[i].startsWith(getString(R.string.bullet))) {
                            lines[i] = getString(R.string.bullet) + " " + lines[i];
                        }
                    }

                    String newText = TextUtils.join("\n", lines);
                    if(charSequence.charAt(charSequence.length() - 1) == '\n') {
                        newText += "\n" + getString(R.string.bullet) + " ";
                    }

                    if (!newText.equals(et_experience.getText().toString())) {
                        // The text has actually changed, so we can safely set it
                        et_experience.setText(newText);
                        et_experience.setSelection(newText.length());
                    }

                }
                rql_exp = charSequence.length();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_skill.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                // Add bullet points when the user starts a new line

                if(rql_skill < charSequence.length()) {
                    String[] lines = charSequence.toString().split("\n");
                    for (int i = 0; i < lines.length; i++) {
                        if (!lines[i].startsWith(getString(R.string.bullet))) {
                            lines[i] = getString(R.string.bullet) + " " + lines[i];
                        }
                    }

                    String newText = TextUtils.join("\n", lines);
                    if(charSequence.charAt(charSequence.length() - 1) == '\n') {
                        newText += "\n" + getString(R.string.bullet) + " ";
                    }

                    if (!newText.equals(et_skill.getText().toString())) {
                        // The text has actually changed, so we can safely set it
                        et_skill.setText(newText);
                        et_skill.setSelection(newText.length());
                    }

                }
                rql_skill = charSequence.length();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        et_other.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int start, int before, int count) {
                // Add bullet points when the user starts a new line

                if(rql_other < charSequence.length()) {
                    String[] lines = charSequence.toString().split("\n");
                    for (int i = 0; i < lines.length; i++) {
                        if (!lines[i].startsWith(getString(R.string.bullet))) {
                            lines[i] = getString(R.string.bullet) + " " + lines[i];
                        }
                    }

                    String newText = TextUtils.join("\n", lines);
                    if(charSequence.charAt(charSequence.length() - 1) == '\n') {
                        newText += "\n" + getString(R.string.bullet) + " ";
                    }

                    if (!newText.equals(et_other.getText().toString())) {
                        // The text has actually changed, so we can safely set it
                        et_other.setText(newText);
                        et_other.setSelection(newText.length());
                    }

                }
                rql_other = charSequence.length();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }



    boolean hasEmptyFields() {
        if(et_title.getText().toString().equals("")) return true;
        if(et_learningData.getText().toString().equals("")) return true;
        if(et_experience.getText().toString().equals("")) return true;
        if(et_skill.getText().toString().equals("")) return true;

        return false;
    }

    private void updateCV() {
        Candidate candidate = ((MyApplication)this.getApplication()).getCandidate_info();

        currentCV.setName(et_title.getText().toString());
        currentCV.setStudy_progress(et_learningData.getText().toString());
        currentCV.setExperiences(et_experience.getText().toString());
        currentCV.setSkills(et_skill.getText().toString());
        currentCV.setOthers(et_other.getText().toString());

        Call<CVResponse> call = cvService.updateCV(currentCV.getId(), currentCV);
        call.enqueue(new Callback<CVResponse>() {
            @Override
            public void onResponse(Call<CVResponse> call, Response<CVResponse> response) {
                if(response.isSuccessful()){
                    onUpdateCVSuccess();
                } else {
                    Toast.makeText(UpdateCvActivity.this, "CV updated fail!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CVResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }

    private void onUpdateCVSuccess() {
        Intent intent = new Intent(UpdateCvActivity.this, ViewAllCVActivity.class);
        Bundle bundle = new Bundle();
        bundle.putString("message", "Update cv successfully!");
        startActivity(intent.putExtras(bundle));
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                this.finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}