package com.example.projectmobile.Adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.projectmobile.Activity.ApplyFormActivity;
import com.example.projectmobile.Activity.UpdateCvActivity;
import com.example.projectmobile.Model.ApplyOffer;
import com.example.projectmobile.Model.CV;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.CVService;

import java.util.List;

public class YourApplyOfferAdapter extends ArrayAdapter<ApplyOffer> {

    private Context context;
    private List<ApplyOffer> applyOffers;

    public YourApplyOfferAdapter(@NonNull Context context, int resource, @NonNull List<ApplyOffer> objects) {
        super(context, resource, objects);
        this.applyOffers = objects;
        this.context = context;
    }

    @Override
    public View getView(final int pos, View convertView, ViewGroup parent){
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_your_job, parent, false);

        //init variable
        TextView tv_title = rowView.findViewById(R.id.tv_name_your_job);
        TextView tv_status = rowView.findViewById(R.id.tv_status_your_job);
        TextView tv_date = rowView.findViewById(R.id.tv_date_your_job);
        LinearLayout ll_your_offer = rowView.findViewById(R.id.ll_your_job);

        //set data
        tv_title.setText(applyOffers.get(pos).getOffer().getTitle());
        int status = applyOffers.get(pos).getApply_status();
        if(status == 0) {
            tv_status.setText("applying");
            tv_status.setTextColor(context.getColor(R.color.yellow));
        } else if(status == 1) {
            tv_status.setText("accepted");
            tv_status.setTextColor(context.getColor(R.color.green_light));
        } else if(status == 2) {
            tv_status.setText("refused");
            tv_status.setTextColor(context.getColor(R.color.red));
        }

        tv_date.setText(applyOffers.get(pos).getCreateDate());

        //event listener
        ll_your_offer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = "You don't have any message from the employer.";
                if(applyOffers.get(pos).getResponse() != null && !applyOffers.get(pos).getResponse().equals("")) {
                    message = "A message from the employer: \n \n" + applyOffers.get(pos).getResponse();
                }
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage(message).setPositiveButton("OK",null).show();
            }
        });
        return rowView;
    }
}
