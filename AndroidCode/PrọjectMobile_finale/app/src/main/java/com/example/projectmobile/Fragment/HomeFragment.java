package com.example.projectmobile.Fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.projectmobile.Adapter.JobOfferAdapter;
import com.example.projectmobile.Model.JobOffer;
import com.example.projectmobile.Model.JobOfferResponse;
import com.example.projectmobile.R;
import com.example.projectmobile.Remote.APIUtils;
import com.example.projectmobile.Remote.JobOfferService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeFragment extends Fragment {

    JobOfferService jobOfferService;
    ListView lv_jobs;
    List<JobOffer> jobOffers = new ArrayList<JobOffer>();;

    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home_page, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        //init variable
        jobOfferService = APIUtils.getJobOfferService();
        lv_jobs = view.findViewById(R.id.lv_job_home);
        //
        getJobOfferList();
    }

    public void getJobOfferList(){
        Call<JobOfferResponse> call = jobOfferService.getAllJobOffers();
        call.enqueue(new Callback<JobOfferResponse>() {
            @Override
            public void onResponse(Call<JobOfferResponse> call, Response<JobOfferResponse> response) {
                if(response.isSuccessful()){
                    jobOffers = response.body().getJobOffers();
                    Collections.reverse(jobOffers);
                    lv_jobs.setAdapter(new JobOfferAdapter(getContext(), R.layout.list_job, jobOffers));
                }
            }

            @Override
            public void onFailure(Call<JobOfferResponse> call, Throwable t) {
                Log.e("ERROR: ", t.getMessage());
            }
        });
    }
}
