const mongoose = require("mongoose");

const abonnementSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, "Please enter the name of abonnement "],
            unique: [true, "abonnement is already existed"],
            dropDups: true,
        },
        description: {
            type: String,
            required: [true, "Please enter the description"]
        },
        price: {
            type: Number,
            required: [true, "Please enter the price"],
            default: 0
        },
        duration: {
            type: Number,
            required: [true, "Please enter the duration"],
            default: 0
        }
    },
    {
        timestamps: true
    }
)

const Abonnement = mongoose.model('Abonnement', abonnementSchema);

module.exports = Abonnement;