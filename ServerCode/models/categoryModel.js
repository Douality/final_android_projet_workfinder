const mongoose = require("mongoose");

const categorySchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, "Please enter the category name"],
            unique: [true, "category is already existed"],
            dropDups: true,
        }
    },
    {
        timestamps: true
    }
)

const Category = mongoose.model('Category', categorySchema);

module.exports = Category;