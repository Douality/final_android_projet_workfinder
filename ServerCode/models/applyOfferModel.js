const mongoose = require("mongoose");

const applyOfferSchema = mongoose.Schema(
    {
        candidat_id: {
            type: String,
            ref: 'Candidat',
            required: true
        },
        offer_id: {
            type: String,
            ref: 'JobOffer',
            required: true
        },
        cv_id: {
            type: String,
            ref: 'CV',
            required: true
        },
        letter_motivation: {
            type: String,
            required: true
        },
        autre_question: {
            type: String,
            required: false
        },
        apply_status: {
            type: Number,
            required: true,
            default: 0
        },
        response: {
            type: String,
            required: false
        },
    },
    {
        timestamps: true
    }
)

const ApplyOffer = mongoose.model('ApplyOffer', applyOfferSchema);

module.exports = ApplyOffer;