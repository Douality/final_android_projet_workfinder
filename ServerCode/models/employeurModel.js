const mongoose = require("mongoose");

const employeurSchema = mongoose.Schema(
    {
        username: {
            type: String,
            unique: [true, "Username is already existed"],
            dropDups: true,
            required: [true, "Please enter the user name"],
            immutable: true
        },
        password: {
            type: String,
            required: [true, "Please enter the password"]
        },
        email: {
            type: String,
            unique: [true, "Email is already existed"],
            dropDups: true,
            required: true
        },
        phone: {
            type: String,
            unique: [true, "Phone is already existed"],
            dropDups: true,
            required: true
        },
        fullname: {
            type: String,
            required: true
        },
        company: {
            type: String,
            required: false
        },
        employeur_role: {
            type: Number,
            required: true,
            default: 0
        },
        location_city: {
            type: String,
            required: false
        },
        location_country: {
            type: String,
            required: false
        },
        company_web: {
            type: String,
            required: false
        }
    },
    {
        timestamps: true
    }
)

const Employeur = mongoose.model('Employeur', employeurSchema);

module.exports = Employeur;