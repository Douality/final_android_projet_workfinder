const mongoose = require("mongoose");

const adminSchema = mongoose.Schema(
    {
        username: {
            type: String,
            unique: [true, "Username is already existed"],
            dropDups: true,
            required: [true, "Please enter the user name"],
            immutable: true
        },
        password: {
            type: String,
            required: [true, "Please enter the password"]
        }
    },
    {
        timestamps: true
    }
)

const Admin = mongoose.model('Admin', adminSchema);

module.exports = Admin;