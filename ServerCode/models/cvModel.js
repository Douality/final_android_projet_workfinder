const mongoose = require("mongoose");

const cvSchema = mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, "Please enter the cv name"]
        },
        candidat_id: {
            type: String,
            ref: 'Candidat'
        },
        study_progress: {
            type: String,
            required: true
        },
        skills: {
            type: String,
            required: true
        },
        experiences: {
            type: String,
            required: true
        },
        others: {
            type: String,
            required: false
        }
    },
    {
        timestamps: true
    }
)

const CV = mongoose.model('CV', cvSchema);

module.exports = CV;