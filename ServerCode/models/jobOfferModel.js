const mongoose = require("mongoose");

const jobOfferSchema = mongoose.Schema(
    {
        employeur_id: {
            type: String,
            ref: 'Employeur'
        },
        category_id: {
            type: String,
            ref: 'Category'
        },
        title: {
            type: String,
            required: true
        },
        description: {
            type: String,
            required: true
        },
        location_city: {
            type: String,
            required: true
        },
        location_country: {
            type: String,
            required: true
        },
        max_salary: {
            type: Number,
            require: true
        },
        min_experience: {
            type: Number,
            require: true
        },
        requirement: {
            type: String,
            required: true
        },
        fromTime: {
            type: Number,
            required: true,
        },
        toTime: {
            type: Number,
            required: true
        },
        duration: {
            type: Number,
            require: true
        }
    },
    {
        timestamps: true
    }
)

const JobOffer = mongoose.model('JobOffer', jobOfferSchema);

module.exports = JobOffer;