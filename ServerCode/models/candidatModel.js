const mongoose = require("mongoose");

const candidatSchema = mongoose.Schema(
    {
        username: {
            type: String,
            dropDups: true,
            unique: [true, "Username is already existed"],
            required: [true, "Please enter the user name"],
            immutable: true
        },
        password: {
            type: String,
            required: [true, "Please enter the password"]
        },
        email: {
            type: String,
            required: true,
            unique: [true, "Email is already existed"],
            dropDups: true,
        },
        phone: {
            type: String,
            unique: [true, "Phone is already existed"],
            dropDups: true,
            required: true
        },
        fullname: {
            type: String,
            required: true
        },
        location_city: {
            type: String,
            required: false
        },
        location_country: {
            type: String,
            required: false
        },
        link_cv: {
            type: String,
            required: false
        }

    },
    {
        timestamps: true
    }
)

const Candidat = mongoose.model('Candidat', candidatSchema);

module.exports = Candidat;