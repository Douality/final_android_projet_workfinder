const mongoose = require("mongoose");

const userAbonnementSchema = mongoose.Schema(
    {
        employeur_id: {
            type: String,
            required: true,
            ref: 'Employeur'
        },
        abonnement_id: {
            type: String,
            required: true,
            ref: 'Abonnement'
        },
        start_date: {
            type: Number,
            required: true,
            default: 0
        },
        out_date: {
            type: Number,
            require: false,
            default: 0
        }
    },
    {
        timestamps: true
    }
)

const UserAbonnement = mongoose.model('UserAbonnement', userAbonnementSchema);

module.exports = UserAbonnement;