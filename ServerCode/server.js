const express = require("express");
const mongoose = require("mongoose");
const app = express();

//import api
const setUpAdminRouteAPI = require("./api_routes/adminRouteAPI");
const setUpCandidatRouteAPI = require("./api_routes/candidatRouteAPI");
const setUpEmployeurRouteAPI = require("./api_routes/employeurRouteAPI");
const setUpCategoryRouteAPI = require("./api_routes/categoryRouteAPI");
const setUpAbonnementRouteAPI = require("./api_routes/abonnementRouteAPI");
const setUpJobOfferRouteAPI = require("./api_routes/jobOfferRouteAPI");
const setUpCVRouteAPI = require("./api_routes/cvRouteAPI");

app.use(express.json());
app.use(express.urlencoded({extended: false}));

//setup routes API
setUpAdminRouteAPI(app);
setUpCandidatRouteAPI(app);
setUpCategoryRouteAPI(app);
setUpAbonnementRouteAPI(app);
setUpEmployeurRouteAPI(app);
setUpJobOfferRouteAPI(app);
setUpCVRouteAPI(app);

//
mongoose.connect("mongodb+srv://testapi:test1234@cluster0.paimz5l.mongodb.net/node-API?retryWrites=true&w=majority").then(() => {
    console.log("Connect to Mongo");
    app.listen(3000, ()=> {
        console.log("API app is running in port 3000");
    })
}).catch((error) => {
    console.log(error);
})