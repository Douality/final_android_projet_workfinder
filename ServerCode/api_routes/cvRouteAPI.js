const CV = require("../models/cvModel");
const Candidat = require("../models/candidatModel");

const setUpCVRouteAPI = (app) => {

    //get a cv
    app.get('/cv/find/:id', async (req, res) => {
        const {id} = req.params;

        try {
            let cv = await CV.findById(id);
            res.status(200).json({code: 200, cv_info: cv});
         
        } catch (error) {
            res.status(200).json({ code: 201 , message: error.message });
        }
    })

    //get all cv of candidat
    app.get('/cv/getAll/:id', async (req, res) => {
        const {id} = req.params;

        try {
            let cvs = await CV.find({"candidat_id": id});
            res.status(200).json({code: 200, list: cvs});
         
        } catch (error) {
            res.status(200).json({ code: 201 , message: error.message });
        }
    })

    //add
    app.post('/cv/add', async (req, res) => {
        try {
            const candidat_id = req.body.candidat_id;
            const candidat = await Candidat.findById(candidat_id);

            if(candidat) {

                const cv = await CV.create(req.body);

                if(cv) {
                    res.status(200).json({ code: 200 , message: "Create cv successfully." });
                } else {
                    res.status(200).json({ code: 201 , message: "Create cv fail." });
                }
            } else {
                
                res.status(200).json({ code: 201 , message: "Candidat is not existed." });
            }

        } catch (error) {
            console.log( error.message );
            res.status(200).json({ code: 201 , message: error.message });
        }
    })
 
    //update
    app.put('/cv/update/:id', async (req, res) => {
        try {
            const {id} = req.params;
            
            delete req.body.candidat_id;

            const cv = await CV.findByIdAndUpdate(id, req.body);
            if (!cv) {
                return res.status(500).json({ code: 201, message: 'cannot find any cv with ID ' + id })
            }

            const updatedCV = await CV.findById(id);
            res.status(200).json({ code: 200, message: 'cv updated.' });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //delete
    app.delete('/cv/delete/:id', async (req, res) => {
        try {
            const {id} = req.params;
            const cv = await CV.findByIdAndDelete(id);
            if (!cv) {
                return res.status(500).json({code: 201, message: 'cannot find any cv with ID ' + id })
            }
            res.status(200).json({ code: 200, message: "Delete successfully" });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

}

module.exports = setUpCVRouteAPI;