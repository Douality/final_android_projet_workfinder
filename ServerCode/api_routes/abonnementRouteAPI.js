const Abonnement = require("../models/abonnementModel");
const UserAbonnement = require("../models/userAbonnementModel");
const Employeur = require("../models/employeurModel");

const setUpAbonnementRouteAPI = (app) => {

    //check abonnement
    app.get('/abonnement/check/:employeur_id', async (req, res) => {
        try {
            const {employeur_id} = req.params;
            const u_abonnement = await UserAbonnement.findOne({"employeur_id": employeur_id, "out_date": 0});

            if(u_abonnement) {
                const abonnement = await Abonnement.findById(u_abonnement.abonnement_id);

                let out_date_time = u_abonnement.start_date + abonnement.duration * 24 * 3600 * 1000; //day
                let current_time = new Date().getTime();

                if(out_date_time > current_time ) {

                    res.status(200).json({ code: 200 , message: "Have abonnement.", out_date_time: out_date_time });

                } else {

                    const u_a_update = await UserAbonnement.findByIdAndUpdate(id, {"out_date": 1});
                    const updated_u_a = await UserAbonnement.findById(id);

                    res.status(500).json({ code: 201 , message: "User does not have any abonnment.", "update": updated_u_a });
                }
                
            } else {
                res.status(500).json({ code: 201 , message: "User does not have any abonnment." });
            }

        } catch (error) {
            res.status(500).json({ code: 201 , message: error.message });
        }
    })

    //make abonnement
    app.post('/abonnement/make', async (req, res) => {
        try {
            const employeur_id = req.body.employeur_id;
            const abonnement_id = req.body.abonnement_id;

            const employeur = await Employeur.findById(employeur_id);
            const abonnement = await Abonnement.findById(abonnement_id);

            if(employeur && abonnement) {
                req.body.start_date = new Date().getTime();
                const u_abonnement = await UserAbonnement.create(req.body);
                
                if(u_abonnement) {
                    res.status(200).json({ code: 200 , message: "Make abonnement successfully." });
                } else {
                    res.status(500).json({ code: 201 , message: "Make abonnement fail." });
                }

            } else {
                res.status(500).json({ code: 201 , message: "Employeur or abonnement is not existed." });
            }

        } catch (error) {
            res.status(500).json({ code: 201 , message: error.message });
        }
    })

    //get all abonnement
    app.get('/abonnement/getAll', async (req, res) => {
        try {
            const abonnement = await Abonnement.find({});
            res.status(200).json({code: 200, list: abonnement});

        } catch (error) {
            res.status(500).json({ code: 201 , message: error.message });
        }
    })

    //add
    app.post('/abonnement/add', async (req, res) => {
        try {
            const abonnement = await Abonnement.create(req.body);

            if(abonnement) {
                res.status(200).json({ code: 200 , message: "Create abonnement successfully." });
            } else {
                res.status(500).json({ code: 201 , message: "Create abonnement fail." });
            }
            

        } catch (error) {
            res.status(500).json({ code: 201 , message: error.message });
        }
    })
 
    //update
    app.put('/abonnement/update/:id', async (req, res) => {
        try {
            const {id} = req.params;
            
            const abonnement = await Abonnement.findByIdAndUpdate(id, req.body);
            if (!abonnement) {
                return res.status(404).json({ code: 201, message: 'cannot find any abonnement with ID ' + id })
            }

            const updatedAbonnement = await Abonnement.findById(id);
            res.status(200).json(updatedAbonnement);

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //delete
    app.delete('/abonnement/delete/:id', async (req, res) => {
        try {
            const {id} = req.params;
            const abonnement = await Abonnement.findByIdAndDelete(id);
            if (!abonnement) {
                return res.status(404).json({code: 201, message: 'cannot find any abonnement with ID ' + id })
            }
            res.status(200).json({ code: 200, message: "Delete successfully" });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

}

module.exports = setUpAbonnementRouteAPI;