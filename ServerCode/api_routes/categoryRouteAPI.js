const Category = require("../models/categoryModel");

const setUpCategoryRouteAPI = (app) => {
    //get all category
    app.get('/category/getAll', async (req, res) => {
        try {
            const category = await Category.find({});
            res.status(200).json({code: 200, list: category});

        } catch (error) {
            res.status(500).json({ code: 201 , message: error.message });
        }
    })

    //add
    app.post('/category/add', async (req, res) => {
        try {
            const category = await Category.create(req.body);

            if(category) {
                res.status(200).json({ code: 200 , message: "Create category successfully." });
            } else {
                res.status(500).json({ code: 201 , message: "Create category fail." });
            }
            

        } catch (error) {
            res.status(500).json({ code: 201 , message: error.message });
        }
    })
 
    //update
    app.put('/category/update/:id', async (req, res) => {
        try {
            const {id} = req.params;
            
            const category = await Category.findByIdAndUpdate(id, req.body);
            if (!category) {
                return res.status(404).json({ code: 201, message: 'cannot find any category with ID ' + id })
            }

            const updatedCategory = await Category.findById(id);
            res.status(200).json(updatedCategory);

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //delete
    app.delete('/category/delete/:id', async (req, res) => {
        try {
            const {id} = req.params;
            const category = await Category.findByIdAndDelete(id);
            if (!category) {
                return res.status(404).json({code: 201, message: 'cannot find any category with ID ' + id })
            }
            res.status(200).json({ code: 200, message: "Delete successfully" });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

}

module.exports = setUpCategoryRouteAPI;