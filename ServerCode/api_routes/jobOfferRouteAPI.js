const ApplyOffer = require("../models/applyOfferModel");
const Candidat = require("../models/candidatModel");
const Category = require("../models/categoryModel");
const Employeur = require("../models/employeurModel");
const JobOffer = require("../models/jobOfferModel");

const setUpJobOfferRouteAPI = (app) => {
    //get all job applied
    app.get('/jobOffer/getAppliedJob/:candidat_id', async (req, res) => {
        try {
            const { candidat_id } = req.params;
            const candidat = await Candidat.findById(candidat_id);

            if (candidat) {
                let applyOffers = await ApplyOffer.find({ "candidat_id": candidat_id }).populate(['offer_id', 'candidat_id', 'cv_id']).populate({ path: "offer_id", populate: ["employeur_id", "category_id"] });

                res.status(200).json({ code: 200, list: applyOffers });
            } else {
                res.status(500).json({ code: 201, message: "Candidat is not existed." });
            }

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //get all applied candidature
    app.get('/jobOffer/getAppliedCandidatures/:offer_id', async (req, res) => {
        try {
            const { offer_id } = req.params;
            const offer = await JobOffer.findById(offer_id);

            if (offer) {
                let applyOffers = await ApplyOffer.find({ "offer_id": offer_id }).populate(['offer_id', 'candidat_id', 'cv_id']).populate({ path: "offer_id", populate: ["employeur_id", "category_id"] });
                res.status(200).json({ code: 200, list: applyOffers });
            } else {
                res.status(500).json({ code: 201, message: "Job offer is not existed." });
            }

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //reponse candidat
    app.put('/jobOffer/response/:id', async (req, res) => {
        try {
            const { id } = req.params;

            delete req.body.candidat_id;
            delete req.body.offer_id;
            delete req.body.cv_id;
            delete req.body.letter_motivation;
            delete req.body.autre_question;

            const appliedOffer = await ApplyOffer.findByIdAndUpdate(id, req.body);
            if (!appliedOffer) {
                return res.status(200).json({ code: 201, message: 'cannot find any applied offer with ID ' + id })
            }

            res.status(200).json({ code: 200, message: 'response successfully'});

        } catch (error) {
            res.status(200).json({ code: 201, message: error.message });
        }
    })

    //apply job
    app.post('/jobOffer/apply', async (req, res) => {
        try {
            const candidat_id = req.body.candidat_id;
            const offer_id = req.body.offer_id;

            const candidat = await Candidat.findById(candidat_id);
            const offer = await JobOffer.findById(offer_id);


            if (candidat && offer) {
                const applyOffer = await ApplyOffer.find({ "candidat_id": candidat_id, "offer_id": offer_id });

                if (applyOffer.length != 0) {
                    res.status(200).json({ code: 201, message: "The candidature used to applied this offer." });

                } else {

                    let current_time = new Date().getTime();
                    if (offer.toTime > current_time) {
                        const applyJobOffer = await ApplyOffer.create(req.body);

                        if (applyJobOffer) {
                            res.status(200).json({ code: 200, message: "Apply job offer successfully." });
                        } else {
                            res.status(200).json({ code: 201, message: "Apply job offer fail." });
                        }
                    } else {
                        res.status(200).json({ code: 201, message: "This offer is outdate." });
                    }
                }

            } else {
                res.status(500).json({ code: 201, message: "Candidat or offer is not existed." });
            }

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //search job offer
    app.get('/jobOffer/search', async (req, res) => {
        try {

            let current_time = new Date().getTime();
            //visible
            const jobOffers = await JobOffer.find(req.body);
            const visibleOffers = jobOffers.filter((job) => {
                return job.toTime > current_time;
            });
            res.status(200).json({ code: 200, list: visibleOffers });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //get posted job
    app.get('/jobOffer/getPostedJob/:employeur_id', async (req, res) => {
        try {
            const { employeur_id } = req.params;
            //visible
            const jobOffers = await JobOffer.find({ "employeur_id": employeur_id }).populate("category_id").populate("employeur_id");

            res.status(200).json({ code: 200, list: jobOffers });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //get all job offer
    app.get('/jobOffer/getAll', async (req, res) => {
        try {
            let current_time = new Date().getTime();
            let jobOffers = await JobOffer.find({}).populate("category_id").populate("employeur_id");
            const visibleOffers = jobOffers.filter((job) => {
                return job.toTime > current_time;
            });
            res.status(200).json({ code: 200, list: visibleOffers });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //add
    app.post('/jobOffer/add', async (req, res) => {
        try {
            const employeur_id = req.body.employeur_id;
            const category_id = req.body.category_id;
            const employeur = await Employeur.findById(employeur_id);
            const category = await Category.findById(category_id);

            if (employeur && category) {
                let current_time = new Date().getTime();
                req.body.fromTime = current_time;
                req.body.toTime = current_time + req.body.duration * 24 * 3600 * 1000; //day

                const jobOffer = await JobOffer.create(req.body);

                if (jobOffer) {
                    res.status(200).json({ code: 200, message: "Create job offer successfully." });
                } else {
                    res.status(500).json({ code: 201, message: "Create job offer fail." });
                }
            } else {

                res.status(500).json({ code: 201, message: "Employeur or category is not existed." });
            }

        } catch (error) {
            console.log(error.message);
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //update
    app.put('/jobOffer/update/:id', async (req, res) => {
        try {
            const { id } = req.params;

            delete req.body.employeur_id;
            delete req.body.category_id;

            const jobOffer = await JobOffer.findByIdAndUpdate(id, req.body);
            if (!jobOffer) {
                return res.status(404).json({ code: 201, message: 'cannot find any job offer with ID ' + id })
            }

            const updatedOffer = await JobOffer.findById(id);
            res.status(200).json(updatedOffer);

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

    //delete
    app.delete('/jobOffer/delete/:id', async (req, res) => {
        try {
            const { id } = req.params;
            const jobOffer = await JobOffer.findByIdAndDelete(id);
            if (!jobOffer) {
                return res.status(404).json({ code: 201, message: 'cannot find any job offer with ID ' + id })
            }
            res.status(200).json({ code: 200, message: "Delete successfully" });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

}

module.exports = setUpJobOfferRouteAPI;