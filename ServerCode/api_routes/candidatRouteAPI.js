const Candidat = require("../models/candidatModel");

const setUpCandidatRouteAPI = (app) => {
    //login
    app.post('/candidat/login', async (req, res) => {
        try {
            if(req.body.username != null && req.body.password != null) {
                const candidat = await Candidat.findOne({"username" : req.body.username, "password" : req.body.password});
                if(candidat) {
                    res.status(200).json({ code: 200 , message: "login successfully.", id: candidat._id, candidat_info: candidat });
                } else {
                    res.status(200).json({ code: 201 , message: "username or password incorrect." });
                }
                
            } else {
                res.status(200).json({ code: 201 , message: "Request body error." });
            }

        } catch (error) {
            res.status(200).json({ code: error.code , message: error.message });
        }
    })

    //register
    app.post('/candidat/register', async (req, res) => {
        try {
            const candidat = await Candidat.create(req.body);

            if(candidat) {
                res.status(200).json({ code: 200 , message: "Create account successfully." });
            } else {
                res.status(200).json({ code: 201 , message: "Create account fail." });
            }
            

        } catch (error) {
            
            if(error.code == 11000) {
                let dup_val = Object.keys(error.keyPattern)[0];
                res.status(200).json({ code: error.code, message: dup_val + " is duplicated." });
                return;
            }

            res.status(200).json({ code: error.code, message: error.message });
        }
    })
 
    //update
    app.put('/candidat/update/:id', async (req, res) => {
        try {
            const {id} = req.params;
            delete req.body.username;
            const candidat = await Candidat.findByIdAndUpdate(id, req.body);
            if (!candidat) {
                return res.status(200).json({ code: 201, message: 'cannot find any candidat with ID ' + id })
            }

            const updatedCandidat = await Candidat.findById(id);
            res.status(200).json({ code: 200, candidat_info: updatedCandidat });

        } catch (error) {
            res.status(200).json({ code: 201, message: error.message });
        }
    })

    //delete
    app.delete('/candidat/delete/:id', async (req, res) => {
        try {
            const {id} = req.params;
            const candidat = await Candidat.findByIdAndDelete(id);
            if (!candidat) {
                return res.status(404).json({code: 201, message: 'cannot find any candidat with ID ' + id })
            }
            res.status(200).json({ code: 200, message: "Delete successfully" });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

}

module.exports = setUpCandidatRouteAPI;