const Admin = require("../models/adminModel");

const setUpAdminRouteAPI = (app) => {
    //login
    app.post('/admin/login', async (req, res) => {
        try {
            if(req.body.username != null && req.body.password != null) {
                const admin = await Admin.findOne({"username" : req.body.username, "password" : req.body.password});
                if(admin) {
                    res.status(200).json({ code: 200 , message: "login successfully.", id: admin._id });
                } else {
                    res.status(500).json({ code: 201 , message: "username or password incorrect." });
                }
                
            } else {
                res.status(500).json({ code: 201 , message: "Request body error." });
            }

        } catch (error) {
            res.status(500).json({ code: 201 , message: error.message });
        }
    })

    //create
    app.post('/admin/add', async (req, res) => {
        try {
            const admin = await Admin.create(req.body);

            if(admin) {
                res.status(200).json({ code: 200 , message: "Create account successfully." });
            } else {
                res.status(500).json({ code: 201 , message: "Create account fail." });
            }
            

        } catch (error) {
            res.status(500).json({ code: 201 , message: error.message });
        }
    })
}

module.exports = setUpAdminRouteAPI;