const Employeur = require("../models/employeurModel");

const setUpEmployeurRouteAPI = (app) => {
    //login
    app.post('/employeur/login', async (req, res) => {
        try {
            if(req.body.username != null && req.body.password != null) {
                const employeur = await Employeur.findOne({"username" : req.body.username, "password" : req.body.password});
                if(employeur) {
                    res.status(200).json({ code: 200 , message: "login successfully.", id: employeur._id, employeur_info: employeur });
                } else {
                    res.status(200).json({ code: 201 , message: "username or password incorrect." });
                }
                
            } else {
                res.status(200).json({ code: 201 , message: "Request body error." });
            }

        } catch (error) {
            res.status(200).json({ code: 201 , message: error.message });
        }
    })

    //register
    app.post('/employeur/register', async (req, res) => {
        try {
            const employeur = await Employeur.create(req.body);

            if(employeur) {
                res.status(200).json({ code: 200 , message: "Create account successfully." });
            } else {
                res.status(200).json({ code: 201 , message: "Create account fail." });
            }
            

        } catch (error) {

            if(error.code == 11000) {
                let dup_val = Object.keys(error.keyPattern)[0];
                res.status(200).json({ code: error.code, message: dup_val + " is duplicated." });
                return;
            }

            res.status(200).json({ code: error.code, message: error.message });
        }
    })
 
    //update
    app.put('/employeur/update/:id', async (req, res) => {
        try {
            const {id} = req.params;
            delete req.body.username;
            const employeur = await Employeur.findByIdAndUpdate(id, req.body);
            if (!employeur) {
                return res.status(200).json({ code: 201, message: 'cannot find any employeur with ID ' + id })
            }

            const updatedEmployeur = await Employeur.findById(id);
            res.status(200).json({ code: 200, employeur_info: updatedEmployeur });

        } catch (error) {
            res.status(200).json({ code: 201, message: error.message });
        }
    })

    //delete
    app.delete('/employeur/delete/:id', async (req, res) => {
        try {
            const {id} = req.params;
            const employeur = await Employeur.findByIdAndDelete(id);
            if (!employeur) {
                return res.status(404).json({code: 201, message: 'cannot find any employeur with ID ' + id })
            }
            res.status(200).json({ code: 200, message: "Delete successfully" });

        } catch (error) {
            res.status(500).json({ code: 201, message: error.message });
        }
    })

}

module.exports = setUpEmployeurRouteAPI;